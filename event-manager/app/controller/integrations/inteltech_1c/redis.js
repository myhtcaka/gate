const redis = require("redis");
const rconf = require('../../../../servconf').redis;

const { promisify } = require('util');
client = redis.createClient(rconf.port, rconf.host);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);
const expireAsync = promisify(client.expire).bind(client);



exports.save_key = async (data) => {
    //    console.log(data);
    let key = 'eve_m:' + data.company_id + ':' + data.type + ':' + data.event + ':' + (await (data.channelid === undefined) ? data.uniqueid : data.channelid);
    let val = data.from + ',' + (await (data.external_num === undefined) ? data.to : data.external_num) + ',' + data.time + ',' + (await (data.sessionid === undefined) ? data.uniqueid : data.sessionid);
    console.log('red key ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.get_key_param = async (from, data) => {
    let key = 'eve_m:' + data.company_id + ':' + data.type + ':' + from + ':' + (await (data.uniqueid === undefined) ? data.sessionid : data.uniqueid);
       console.log('-get_key_param '+key);
    let dat = await getAsync(key);
    if (dat) {
        let send = {
            "from": dat.split(',')[0],
            "to": dat.split(',')[1],
            "time": dat.split(',')[2],
            "sessionid": dat.split(',')[3]
        }
        return send;
    }
    else { return null; }
}
