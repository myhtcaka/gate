const app = require("express")();
const fs = require('fs');
const https = require('https');
const bodyParser = require('body-parser');

const set = require('./servconf').conf;
const ami_sip = require('./app/router/ami_sip');
const gate_back = require('./app/router/gate_back');
const event_manager = require('./app/router/event_manager');
const doctor_eleks = require('./app/router/doctor_eleks');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({ type: 'text/html' }));

app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Authorization ,X-CSRF-Token,X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
    res.header("Access-Control-Allow-Methods", "POST, GET");
    //  console.log(req);
    next();
});


app.use('/ami_sip', ami_sip);
app.use('/gate_back', gate_back);
app.use('/event_manager', event_manager);
app.use('/doctor_eleks', doctor_eleks);


https.createServer({
    key: fs.readFileSync(set.cer.key),
    cert: fs.readFileSync(set.cer.cert)
}, app)
    .listen(set.post, set.host, function () {
        console.log('App listening go to https://' + set.host + ':' + set.post);
    });