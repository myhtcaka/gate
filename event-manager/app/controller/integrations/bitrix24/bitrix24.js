// bitrix24 v 0.1.1
const redis = require('./redis');
const key = require('./keys');
const dateFormat = require('dateformat');
const request = require('request-promise');

exports.bitrix24 = async (all_data) => {
     console.log('-bitrix24 all_data--- ', all_data);


if ((all_data.event == 'Track') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}
else if ((all_data.event == 'ClickToCall') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}
else if ((all_data.event == 'ClickToCall_D') && (all_data.type == 'out')) {
   await redis.save_click('ClickToCall_D', all_data);
}

 
else if ((all_data.event == 'CRMStart') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
    let param_t = await redis.get_key_param('Track', all_data);
            let options = {
                "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.register",
                qs: 
                { 
                  USER_PHONE_INNER: (all_data.to).slice(-3),
                  PHONE_NUMBER: all_data.from,
                  TYPE: 2,
                  LINE_NUMBER: param_t.to,
                  SHOW: 1, 
                },
                "headers":
                {
                    "Content-Type": all_data.int.content_type
                },
                "method": "POST",
                "json": true
    
            };
            let result = await this.bitrix24_send(options);
            if (result)
            {            
               await redis.save_res('bitrix24',result, all_data)
            }
            let user_id = {
                "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/user.get",
                qs: 
                { 
                    UF_PHONE_INNER: all_data.to.slice(-3),
                },
                "headers":
                {
                    "Content-Type": all_data.int.content_type
                },
                "method": "POST",
                "json": true
            };
            //console.log('-----------------------SEND USEEEEEEEEER', user_id);
            
            let user = await this.bitrix24_send(user_id);
            if (user)
            {
               //console.log('--------------------------------START USEEEEEEEEEER', user);
               
               await redis.save_user('user', user, all_data);
            }
    else {
        console.log('------------------------------no track (out call)--------------------------------');
    }
}   


else if ((all_data.event == 'Answer') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}

else if ((all_data.event == 'Hangup') && (all_data.type == 'in')  && (all_data.cause == 16)) {
    await redis.save_key(all_data);
    let param_a = await redis.get_key_param('Answer', all_data);
    //console.log('param_a ', param_a);
    if (param_a) {
        let param_сс = await redis.get_key_param('ClickToCall', all_data);
        if (param_сс){
            let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(param_сс.sessionid);
            let fname = await key.get_audio_key2(param_сс.sessionid);
            let call_id = await redis.get_click_param('ClickToCall_D', param_сс);
            let call = call_id.accountcode;
            //console.log('----------------------------------------NEW CALL ', call);

            let user_id = {
                "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/user.get",
                qs: 
                { 
                    UF_PHONE_INNER: all_data.to.slice(-3),
                },
                "headers":
                {
                    "Content-Type": all_data.int.content_type
                },
                "method": "POST",
                "json": true
            };
            let user = await this.bitrix24_send(user_id);
            if (user)
            {
               //console.log('---------------get user ID with data ', user);
               await redis.save_user('user', user, all_data);
            } else {
                console.log('---------------Cannot get user ID with data ', all_data);
            }

            //return call;

            let userid = await redis.get_user_param('user', all_data);
            let options = {
                "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
                qs: 
                { 
                    CALL_ID: call,
                    USER_ID: userid.ID,
                    DURATION: (all_data.time).toString().slice(0,-3) - (param_a.time).toString().slice(0,-3),
                    RECORD_URL: mp3,
                    STATUS_CODE: '200',
                    ADD_TO_CHAT: '1',
                },
                "headers":
                {
                    "Content-Type": all_data.int.content_type
                },
                "method": "POST",
                "json": true
    
            };            
            console.log('Hangup Answer bitrix24_send ', options);
            await this.bitrix24_send(options);

            
        } else {
            let param_c = await redis.get_key_param('CRMStart', all_data);
            let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(param_c.sessionid);
            let fname = await key.get_audio_key2(param_c.sessionid);
            let res = await redis.get_res_param('bitrix24', all_data);
            let call = res.CALL_ID;
            //console.log('----------------------------------------OLD CALL ', call);

             //return call;

             let user = await redis.get_user_param('user', all_data);
             //console.log('----------------------------------------OLD CALL USER ', user);
             let options = {
                 "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
                 qs: 
                 { 
                     CALL_ID: call,
                     USER_ID: user.ID,
                     DURATION: (all_data.time).toString().slice(0,-3) - (param_a.time).toString().slice(0,-3),
                     RECORD_URL: mp3,
                     STATUS_CODE: '200',
                     ADD_TO_CHAT: '1',
                 },
                 "headers":
                 {
                     "Content-Type": all_data.int.content_type
                 },
                 "method": "POST",
                 "json": true
     
             };            
             console.log('Hangup Answer bitrix24_send ', options);
             await this.bitrix24_send(options);
        }
    } else {
        let res = await redis.get_res_param('bitrix24', all_data);
        let user = await redis.get_user_param('user', all_data);
        //let chan_id = await redis.get_hang_param('CRMStart', all_data);
        //console.log('---------------------------------------------RESSSSSSS ', res);
        //console.log('---------------------------------------------USER ', user);
        let options = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
            qs: 
            { 
                CALL_ID: res.CALL_ID,
                USER_ID: user.ID,
                ADD_TO_CHAT: '1',
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true
        };
        console.log('Answer bitrix24_send ', await this.bitrix24_send(options));

    }
} else if ((all_data.event == 'Hangup') && (all_data.type == 'in') && (all_data.cause == 26)) {

    //console.log('########################-------------------- ALL DATA CAUSE not answered: ' + all_data.cause);            
    let res = await redis.get_res_param('bitrix24', all_data);
    let user = await redis.get_user_param('user', all_data);
    let options = {
        "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.hide",
        qs: 
        { 
            CALL_ID: res.CALL_ID,
            USER_ID: user.ID,
        },
        "headers":
        {
            "Content-Type": all_data.int.content_type
        },
        "method": "POST",
        "json": true
    };
    console.log('Hide bitrix24_send ', await this.bitrix24_send(options));
} 
else if ((all_data.event == 'Hangup') && (all_data.type == 'in') && (all_data.cause == 17)) {

    let param_сс = await redis.get_key_param('ClickToCall', all_data);
    if (param_сс){
        let call_id = await redis.get_click_param('ClickToCall_D', param_сс);
        let call = call_id.accountcode;
        console.log('NEW CALL ', call);

        let user_id = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/user.get",
            qs: 
            { 
                UF_PHONE_INNER: all_data.to.slice(-3),
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true
        };
        let user = await this.bitrix24_send(user_id);
        if (user)
        {
           //console.log('---------------save user ID with data ', user);
           await redis.save_user('user', user, all_data);
        } else {
            console.log('---------------Cannot get user ID with data ', all_data);
        }

        //return call;

        let userid = await redis.get_user_param('user', all_data);
        //console.log('_____________________________USER_ID________ ', userid);
        
        let options = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
            qs: 
            { 
                CALL_ID: call,
                USER_ID: userid.ID,
                FAILED_REASON: 'Manager is busy',
                STATUS_CODE: 486,
                ADD_TO_CHAT: '1',
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true

        };            
        console.log('Hangup Answer bitrix24_send ', options);
        await this.bitrix24_send(options);

        
    } else {
        let param_c = await redis.get_key_param('CRMStart', all_data);
        let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(param_c.sessionid);
        let fname = await key.get_audio_key2(param_c.sessionid);
        let res = await redis.get_res_param('bitrix24', all_data);
        let call = res.CALL_ID;
        //console.log('----------------------------------------OLD CALL ', call);

         //return call;

         let user = await redis.get_user_param('user', all_data);
         //console.log('----------------------------------------OLD CALL USER ', user);
         let options = {
             "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
             qs: 
             { 
                 CALL_ID: call,
                 USER_ID: user.ID,
                 FAILED_REASON: 'Manager is busy',
                 STATUS_CODE: 486,
                 ADD_TO_CHAT: '1',
             },
             "headers":
             {
                 "Content-Type": all_data.int.content_type
             },
             "method": "POST",
             "json": true
 
         };            
         console.log('Hangup Answer bitrix24_send ', options);
         await this.bitrix24_send(options);
    }
}

//!------------------------out----------------------

else if ((all_data.event == 'CRMStart') && (all_data.type == 'out')) {
    //console.log( 'CRMStart out bitrix24 ',all_data);
    //console.log('StartCall out bitrix24_save ');
    await redis.save_key(all_data);
        let options = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.register",
            qs: 
            { 
              USER_PHONE_INNER: (all_data.from).slice(-3),
              PHONE_NUMBER: all_data.to,
              TYPE: 1,
              SHOW: 1, 
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true
        };
        //console.log('StartCall in bitrix24_send ');
        let result = await this.bitrix24_send(options);
        if (result)
        {
           await redis.save_res('bitrix24',result, all_data)
           //console.log('------------- -------------- Start OUT ', result);
           
        }

        let user_id = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/user.get",
            qs: 
            { 
                UF_PHONE_INNER: all_data.from.slice(-3)
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true

        };
        //console.log('StartCall in bitrix24_send to user');
        let user = await this.bitrix24_send(user_id);
        if (user)
        {
            //console.log('----------------ID', user.result[0].ID);
           await redis.save_user('user',user, all_data);
        }
}

else if ((all_data.event == 'Answer') && (all_data.type == 'out')) {
    //console.log('################DATA TO SAVE: ', all_data);
    await redis.save_key(all_data);
}


 else if ((all_data.event == 'Hangup') && (all_data.type == 'out')) {
     await redis.save_key(all_data);
     let param_a = await redis.get_key_param('Answer', all_data);
     if (param_a) {
            let param_c = await redis.get_key_param('CRMStart', all_data);
            //console.log('-- send out Hangup A ', param_a);
            //console.log('-- send out Hangup C ', param_c);
            //console.log('-- send out Hangup C ', param_c);
            console.log('out Hangup A ', all_data);
         let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(all_data.uniqueid);

        let res = await redis.get_res_param('bitrix24', all_data);
        let user = await redis.get_user_param('user', all_data);
        console.log(' CLICK2CALL USER ', user);
        console.log('CLICK2CALL RES ', res);
        if (user && res){
        //console.log('Hangup res ',res);
        //console.log('Hangup user ',user);
         let options = {
             "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
             qs: 
             { 
                 CALL_ID: res.CALL_ID,
                 USER_ID: user.ID,
                 DURATION: (all_data.time).toString().slice(0,-3) - (param_a.time).toString().slice(0,-3),
                 RECORD_URL: mp3,
                 STATUS_CODE: '200',
                 ADD_TO_CHAT: '1',
             },
             "headers":
             {
                 "Content-Type": all_data.int.content_type
             },
             //body,
             "method": "POST",
             "json": true
         };
         console.log('Hangup out bitrix24_send ', await this.bitrix24_send(options));
        } else {
            console.log('------------------------ CLICK2CALL END ');
        }
        
     }
     else if ((all_data.event == 'Hangup') && (all_data.type == 'out') && (all_data.cause == 17)) {

        //!console.log('########################-------------------- ALL DATA CAUSE not answered: ' + all_data.cause);            
        let res = await redis.get_res_param('bitrix24', all_data);
        let user = await redis.get_res_param('user', all_data);
        if (user && res){
        //let chan_id = await redis.get_hang_param('CRMStart', all_data);
        //console.log('---------------------------------------------CHANNEL_ID AND TIME: ', chan_id);
        let options = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
            qs: 
            { 
                CALL_ID: res.CALL_ID,
                USER_ID: user.CALL_ID,
                STATUS_CODE: '486'
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true
        };
        console.log('Hide bitrix24_send ', await this.bitrix24_send(options));
    } else {
        console.log('CLICK2CALL END ');
    }
    
    }
    else if ((all_data.event == 'Hangup') && (all_data.type == 'out') && (all_data.cause == 21)) {
        //console.log('########################-------------------- ALL DATA CAUSE not answered: ' + all_data.cause);            
        let res = await redis.get_res_param('bitrix24', all_data);
        let user = await redis.get_res_param('user', all_data);
        console.log('CLICK2CALL USER ', user);
        console.log('CLICK2CALL RES ', res);
        if (user && res){
        //let chan_id = await redis.get_hang_param('CRMStart', all_data);
        //console.log('---------------------------------------------CHANNEL_ID AND TIME: ', chan_id);
        let options = {
            "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
            qs: 
            { 
                CALL_ID: res.CALL_ID,
                USER_ID: user.CALL_ID,
                STATUS_CODE: '503',
                ADD_TO_CHAT: '1',
            },
            "headers":
            {
                "Content-Type": all_data.int.content_type
            },
            "method": "POST",
            "json": true
        };
        console.log('Hide bitrix24_send ', await this.bitrix24_send(options));
    } else {
        console.log('CLICK2CALL END ');
    }
        
     
    }
     else {
            let param_c = await redis.get_key_param('CRMStart', all_data);
            let res = await redis.get_res_param('bitrix24', all_data);
            let user = await redis.get_user_param('user', all_data);
            console.log('CLICK2CALL USER ', user);
            console.log('CLICK2CALL RES ', res);
            if (user && res){
            // ((res !== null || res !== undefined) || (user !== null || user !== undefined)){
                let options = {
                    "url": all_data.int.web_hook + JSON.parse(all_data.int.client_key).b24.in_k + "/telephony.externalcall.finish",
                    qs: 
                    { 
                        CALL_ID: res.CALL_ID,
                        USER_ID: user.ID,
                        STATUS_CODE: '200',
                        ADD_TO_CHAT: '1',
                    },
                         "headers":
                         {
                             "Content-Type": all_data.int.content_type
                         },
                         "method": "POST",
                         "json": true
                     };
                     console.log('NO Answer bitrix24_send ', await this.bitrix24_send(options));
            } else {
                console.log('CLICK2CALL END ');
            }
     }
 } 
 
} //Do not removethis bracket 
exports.bitrix24_send = async (options) => {
     console.log(' --- bitrix24_send options-- ',options.body);
     console.log(' --- bitrix24_send options .events-- ', options);
    try {
        const response = await request(options);
        console.log('Resp from BITRIX24 ' + response);
        return response;
    }
    catch (error) {
        console.log(error.message);
        return null;
    }
}
