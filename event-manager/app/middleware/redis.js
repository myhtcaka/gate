const redis = require("redis");
const rconf = require('../../servconf').redis;

const { promisify } = require('util');
client = redis.createClient(rconf.port, rconf.host);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);
const expireAsync = promisify(client.expire).bind(client);


exports.save_keys_sip = async (data) => {
 
    if(data.a_marker==='start_in'){
        let key = data.app +':'+ data.company_id + ':' + data.a_marker + ':' + data.sessionid+':'+ data.sessionid;
        let val = JSON.stringify(data);
        let add = await setAsync(key, val);
        let exp = await expireAsync(key, rconf.key_exp);
  //      console.log('-- start_in key ' + key);
        
    }else if(data.a_marker==='ClickToCall'){
        let key = data.app +':'+ data.company_id + ':start_out:' + data.sessionid+':'+ data.sessionid;
        let key_2 = data.app +':'+ data.company_id + ':clicktocall:' + data.sessionid+':'+ data.sessionid;
        let val = JSON.stringify(data);
        let add = await setAsync(key, val);
        let exp = await expireAsync(key, rconf.key_exp);
        let add_2 = await setAsync(key_2, val);
        let exp_2 = await expireAsync(key_2, rconf.key_exp);
     //   console.log(JSON.stringify(data));
        

    } else if(data.a_marker==='start_out'){
        let tr_k = await this.get_t_key(data);
        let f_tr_k = await this.get_key(data,'start_out',data.sessionid,data.sessionid)
        if (tr_k){
  //          console.log('--- tr_k ',tr_k);
            
         let key_2 = data.app +':'+ data.company_id + ':transfer:' +':'+ data.sessionid+':' + data.sessionid+':'+(Number((tr_k.pop()).split('transfer:'+data.sessionid+':')[1])+1);
            await this.cre_t_key(key_2,JSON.stringify(data));
  //          console.log('--- key_2 ',key_2);
            
            return 1;
        } else if(f_tr_k){
       //     console.log(' out start_out first trans ',JSON.stringify(data));
            let key_1 = data.app +':'+ data.company_id + ':transfer:' + data.sessionid+':'+ data.sessionid+ ':1';
            await this.cre_t_key(key_1,JSON.stringify(data));
            return 1;
        } else {
            let key = data.app +':'+ data.company_id +':'+ data.a_marker +':' + data.sessionid+':' + data.sessionid;
            let val = JSON.stringify(data);
            let add = await setAsync(key, val);
            let exp = await expireAsync(key, rconf.key_exp);
           // console.log(' out start_out first ',JSON.stringify(data));
   //         console.log('-- start_out key ' + key);
            return null;
        }
    

    } else if (data.a_marker==='ring'){

        if(data.sessionid===data.callid){
           if(data.company_source==='callerid_ext'){
            //   console.log('--1-- ring key callerid_ext');
               let key = data.app +':'+ data.company_id +':'+ data.a_marker+':'+ data.sessionid+ ':'+data.sessionid ;
               let val = JSON.stringify(data);
           let add = await setAsync(key, val);
           let exp = await expireAsync(key, rconf.key_exp);
  //         console.log('-- ring key ' + key);
       }
            let key = data.app +':'+ data.company_id +':'+ data.a_marker+':'+ data.channelid+ ':'+data.sessionid ;
            let val = JSON.stringify(data);
        let add = await setAsync(key, val);
        let exp = await expireAsync(key, rconf.key_exp);
 //       console.log('-- ring key ' + key);
        }else {
          //  if(data.company_source==='callerid_ext'){console.log('--2-- ring key callerid_ext');
       // }
            let key_1 = data.app +':'+ data.company_id +':' +data.a_marker + ':'+ data.channelid+':'+data.sessionid;
            let key_2 = data.app +':'+ data.company_id + ':'+ data.a_marker + ':' + data.callid+':'+data.sessionid;
            let val = JSON.stringify(data);
   //        console.log('-- ring key ' + key_1);
            //  + ' | val ' + val);
   //        console.log('-- ring key ' + key_2);
            // + ' | val ' + val);
        let add_1 = await setAsync(key_1, val);
        let exp_1 = await expireAsync(key_1, rconf.key_exp);
        let add_2 = await setAsync(key_2, val);
        let exp_2 = await expireAsync(key_2, rconf.key_exp);
     //   return 1;

        }
    } else if ((data.a_marker==='answer')||(data.a_marker==='hangup')){
        let k_r = await this.get_key(data,'ring',data.uniqueid,'*');
        // console.log('=========== k_r -H - A ',k_r);
        if (k_r) {
            // .split('transfer:'+data.sessionid+':')[1]
            let key = data.app +':'+ data.company_id + ':' + data.a_marker + ':' + data.uniqueid+':'+k_r.split(data.uniqueid+':')[1];
        let val = JSON.stringify(data);

    let add = await setAsync(key, val);
    let exp = await expireAsync(key, rconf.key_exp);
  //  console.log('-- '+data.a_marker+'  key ' + key);
            
        }else{
  //              console.log(' redis no save answer key no ring ');
                
        }
        
        

    } 
 }


 exports.get_key  = async (ev,part,key_c,key_s)=>{
      
    let key = ev.app +':'+ ev.company_id + ':'+part+':'+ key_c+':' + key_s;
    let t_data = await keysAsync(key);
    if(t_data.length > 0){
   //     console.log(' redis get_keys ',t_data);
        
        return t_data.pop();
    } else {
        return null;
    }
}

exports.get_t_key  = async (e_s)=>{
      
    let key = e_s.app +':'+ e_s.company_id + ':transfer:'+ e_s.sessionid +':'+ e_s.sessionid+ ':*';
    let t_data = await keysAsync(key);
    if(t_data.length > 0){
        return t_data;
    } else {
        return null;
    }
}

exports.cre_t_key = async (key,ev)=>{
    let add = await setAsync(key, ev);
    let exp = await expireAsync(key, rconf.key_exp);

}


exports.save_keys_start_in_sip = async (data) => {
    
    if(data.a_marker==='start_in'){
        let key = data.app +':'+ data.company_id + ':' + data.a_marker + ':' + data.sessionid+':'+ data.sessionid;
        let val = JSON.stringify(data);
        let add = await setAsync(key, val);
        let exp = await expireAsync(key, rconf.key_exp);
 //       console.log('-- start_in key ' + key);
        
    } 
}