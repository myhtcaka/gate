const options = require('../../../servconf').mariadb;
const mariadb = require('mariadb');
const bunyan = require('bunyan');
const bunyan_conf = require('../../../servconf').bunyan;

exports.all_internal = async () => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.id, crm.companies.softswitch_user_group_id,crm.ss_users_phones.internal_phone FROM crm.companies, crm.ss_users_phones where crm.ss_users_phones.user_group_id = crm.companies.softswitch_user_group_id and crm.ss_users_phones.internal_phone like '8004%'");
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- all_internal handle connection error');
      return null;

    });
}

exports.crm_id_internal = async (crm_id) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.id, crm.companies.softswitch_user_group_id,crm.ss_users_phones.internal_phone FROM crm.companies, crm.ss_users_phones where crm.ss_users_phones.user_group_id = crm.companies.softswitch_user_group_id and crm.ss_users_phones.internal_phone like '8004%' and crm.companies.id=?", [crm_id]);
      conn.end();
      return rows;
    }).catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- crm_id_internal handle connection error');
      return null;
    });
}

// user internal phone, by user id (doctor)
exports.user_internal_by_uid = async (uid) => {
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.users.internal_phone from crm.users where crm.users.id=?", [uid]);
      conn.end();
      return rows;
    }).catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- user_internal_by_uid connection error');
      return null;
    });

}

exports.active_channel_uniqueid = async (uniqueid) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.ss_active_calls.company_id, crm.ss_active_calls.channel from crm.ss_active_calls WHERE crm.ss_active_calls.uniqueid=?", [uniqueid]);
      conn.end();
      return rows;
    }).catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- active_channel_uniqueid handle connection error');
      return null;
    });
}



exports.audio_record_param = async (data) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT telecom.cdr_replica_ready.id, telecom.cdr_replica_ready.user_group_id, telecom.cdr_replica_ready.session_id ,telecom.cdr_replica_ready.has_call_record,start_time FROM telecom.cdr_replica_ready WHERE telecom.cdr_replica_ready.session_id=?", [data]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- audio_record_param handle connection error');
      return null;
    });
}

exports.all_external = async () => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.id, crm.companies.softswitch_user_group_id,crm.external_numbers.name FROM crm.companies, crm.external_numbers where crm.external_numbers.company_id = crm.companies.id and crm.companies.softswitch_user_group_id<>0");
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ----------  all_external handle connection error');
      return null;
    });
}

exports.softswitch_by_key = async (key) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.id,crm.companies.softswitch_user_group_id,crm.companies.softswitch_login, crm.companies.softswitch_password from crm.companies where crm.companies.api_key=?", [key]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- softswitch_by_key handle connection error');
      return null;
    });
}

exports.roles_wizard_by_key = async (key) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.name,crm.companies.id,crm.companies.softswitch_user_group_id, crm.companies.roles_wizard_id FROM crm.companies WHERE crm.companies.api_key=?", [key]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- roles_wizard_by_key handle connection error');
      return null;
    });
}

exports.lic_extifces_by_key = async (key) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.name,crm.companies.id,crm.companies.softswitch_user_group_id, crm.companies.softswitch_login, crm.companies.softswitch_password, crm.companies.lic_max_extifces FROM crm.companies WHERE  crm.companies.api_key=?", [key]);
      conn.end();
      //      console.log(rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- lic_extifces_by_key handle connection error');
      return null;
    });
}

//exports.get_integrations = async () => {
//  let log = bunyan.createLogger(bunyan_conf);
//  return await mariadb.createConnection(options)
//    .then(async conn => {
//      let rows = await conn.query("SELECT db_integration.integration_type.name, db_integration.integration_companys.company_id, db_integration.integration_parameters.content_type, db_integration.integration_parameters.web_hook, db_integration.integration_parameters.client_key FROM db_integration.integration_parameters, db_integration.integration_companys, db_integration.integration_type WHERE db_integration.integration_type.id=db_integration.integration_parameters.int_type and db_integration.integration_parameters.int_comp_id=db_integration.integration_companys.id and db_integration.integration_parameters.status='1' and db_integration.integration_type.voip='1'");
//      conn.end();
//      console.log('--------- rows ---------- get_integrations', rows);
//      return rows;
//    })
//    .catch(err => {
//      log.error(Date.now(), err);
//      console.log('--------- to log ---------- get_integrations handle connection error');
//      return null;
//    });
//
//}

exports.get_integrations = async () => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT db_integration.integration_type.name, db_integration.integration_companys.company_id, db_integration.integration_parameters.content_type, db_integration.integration_parameters.web_hook, db_integration.integration_parameters.client_key,db_integration.integration_parameters.partner_key FROM db_integration.integration_parameters, db_integration.integration_companys, db_integration.integration_type WHERE db_integration.integration_type.id=db_integration.integration_parameters.int_type and db_integration.integration_parameters.int_comp_id=db_integration.integration_companys.id and db_integration.integration_parameters.status='1' and db_integration.integration_type.voip='1'");
      conn.end();
      console.log('--------- rows ---------- get_integrations', rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- get_integrations handle connection error');
      return null;
    });

}

exports.get_integrations_doctorEleks = async () => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn1 => {
      let rows = await conn1.query("select db_integration.integration_companys.company_id, db_integration.integration_companys.company_name,crm.companies.api_key ,db_integration.integration_parameters.client_login, db_integration.integration_parameters.client_key, db_integration.integration_parameters.client_pass, db_integration.integration_parameters.crmstream_login, db_integration.integration_parameters.crmstream_pass, db_integration.integration_parameters.crmstream_key, db_integration.integration_parameters.web_hook, db_integration.integration_parameters.content_type FROM db_integration.integration_parameters,crm.companies ,db_integration.integration_companys WHERE db_integration.integration_companys.id= db_integration.integration_parameters.int_comp_id and crm.companies.id=db_integration.integration_companys.id and db_integration.integration_parameters.int_type='24' and db_integration.integration_parameters.status='1' ");
      conn1.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- get_integrations_doctorEleks handle connection error');
      return null;
    });

}

// exports.streamtele_change_clients = async (id_company, t_from, t_to) => {
//   let log = bunyan.createLogger(bunyan_conf);
//   return await mariadb.createConnection(options)
//     .then(async conn => {
//       let rows = await conn.query("select crm.customers.id, crm.customers.name, crm.customers.family_name, crm.customers.second_name, crm.customers.email, crm.customers.address, crm.customers.sex, crm.customers.birth_date, crm.customers.information_channel_id  FROM crm.customers where crm.customers.company_id=? and crm.customers.rowstatus_id<2 and crm.customers.rowtype_id=2 and  crm.customers.last_changes>? and crm.customers.last_changes<?", [id_company, t_from, t_to]);
//       conn.end();
//       return rows;
//     })
//     .catch(err => {
//       log.error(Date.now(), err);
//       console.log('--------- to log ---------- streamtele_change_clients handle connection error');
//       return null;
//     });
// }

exports.streamtele_get_doctoreleks_id = async (customer_id) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.custom_rows_data.value FROM crm.custom_rows_data WHERE crm.custom_rows_data.custom_row_id='2798' and crm.custom_rows_data.customer_id=?", [customer_id]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_doctoreleks_id handle connection error');
      return null;
    });
}


exports.streamtele_get_clients_phones = async (customer_id) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.customers_phones.phone FROM crm.customers_phones WHERE crm.customers_phones.customer_id=?", [customer_id]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_clients_phones handle connection error');
      return null;
    });
}


exports.streamtele_get_client_info_channels = async (info_channels_id) => {
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.information_channels.name from crm.information_channels WHERE crm.information_channels.id=?", [info_channels_id]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_client_info_channels handle connection error');
      return null;
    });
}


// данные измененых клиентов с streamCRM (для доктора)
exports.streamtele_change_clients = async (id_company, t_from, t_to) => {
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("select crm.customers.id, crm.customers.name, crm.customers.family_name, crm.customers.second_name, crm.customers.email, crm.customers.address, crm.customers.sex, DATE_FORMAT(crm.customers.birth_date,'%Y-%m-%d') AS 'birth_date', crm.customers.information_channel_id, crm.customers.insert_by_user_id, crm.customers.last_changes_user_id FROM crm.customers where crm.customers.company_id=? and crm.customers.rowstatus_id<2 and crm.customers.rowtype_id=2 and  crm.customers.last_changes>? and crm.customers.last_changes<?", [id_company, t_from, t_to]);
      conn.end();
      console.log('--streamtele_change_clients ', rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ----------   handle connection error', err);
      return null;
    });
}





// все пользователи с параметрами по имени интеграции
exports.all_users_params_by_integretions = async (int_name) => {
  //  console.log('all_users_params_by_integretions int_name ',int_name);

  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT db_integration.integration_parameters.client_key, crm.companies.softswitch_login, crm.companies.softswitch_password, db_integration.integration_companys.company_id, crm.companies.softswitch_user_group_id, crm.companies.api_key FROM db_integration.integration_companys,db_integration.integration_parameters, db_integration.integration_type, crm.companies WHERE db_integration.integration_type.id=db_integration.integration_parameters.int_type and db_integration.integration_parameters.int_comp_id=db_integration.integration_companys.id and db_integration.integration_companys.company_id=crm.companies.id and db_integration.integration_type.name=?", [int_name]);
      conn.end();
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_client_info_channels handle connection error');
      return null;
    });
}

// 
exports.integretions_params_by_crm_id = async (crm_id) => {
  //  console.log('all_users_params_by_integretions int_name ',int_name);

  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT db_integration.integration_type.name, db_integration.integration_parameters.client_key,db_integration.integration_parameters.web_hook, crm.companies.softswitch_login, crm.companies.softswitch_password, db_integration.integration_companys.company_id, crm.companies.softswitch_user_group_id, crm.companies.api_key, crm.companies.super_stickiness_minutes FROM db_integration.integration_companys,db_integration.integration_parameters, db_integration.integration_type, crm.companies WHERE db_integration.integration_type.id=db_integration.integration_parameters.int_type and db_integration.integration_parameters.int_comp_id=db_integration.integration_companys.id and db_integration.integration_companys.company_id=crm.companies.id and db_integration.integration_parameters.status='1' and crm.companies.id=?", [crm_id]);
      conn.end();
      // console.log('integretions_params_by_crm_id ',rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_client_info_channels handle connection error');
      return null;
    });
}

exports.integretions_params_by_key = async (api_key) => {
  console.log('integretions_params_by_key api_key ', api_key);

  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT db_integration.integration_type.name, db_integration.integration_parameters.client_key,db_integration.integration_parameters.web_hook, crm.companies.softswitch_login, crm.companies.softswitch_password, db_integration.integration_companys.company_id, crm.companies.softswitch_user_group_id, crm.companies.api_key, crm.companies.super_stickiness_minutes FROM db_integration.integration_companys,db_integration.integration_parameters, db_integration.integration_type, crm.companies WHERE db_integration.integration_type.id=db_integration.integration_parameters.int_type and db_integration.integration_parameters.int_comp_id=db_integration.integration_companys.id and db_integration.integration_companys.company_id=crm.companies.id and db_integration.integration_parameters.status='1' and crm.companies.api_key=?", [api_key]);
      conn.end();
      console.log('integretions_params_by_crm_id ', rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_client_info_channels handle connection error');
      return null;
    });
}

// stickylong = async (crm_id,num) => {
exports.super_stickiness_minutes = async (crm_id) => {
  //  console.log('all_users_params_by_integretions int_name ',int_name);
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.companies.super_stickiness_minutes FROM crm.companies WHERE crm.companies.id = ?", [crm_id]);
      conn.end();
      // console.log('super_stickiness_minutes ',rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_client_info_channels handle connection error');
      return null;
    });
}

// stickylong

exports.stickylong = async (crm_id, num) => {
  //  console.log('all_users_params_by_integretions int_name ',int_name);
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.users.internal_phone, crm.users.realname AS manRealName, crm.customers.family_name AS client_F, crm.customers.name AS client_I, crm.customers.second_name AS client_O, crm.customers.company_name AS client_C, crm.customers.common_view_fl AS clientCommnon from crm.users, crm.customers,crm.customers_phones where crm.customers.owner_user_id=crm.users.id and crm.customers.rowstatus_id = '1' and crm.customers.id=crm.customers_phones.customer_id and crm.customers.company_id=? and crm.customers_phones.phone LIKE ? UNION SELECT crm.last_contact.mng_num AS internal_phone, '' AS manRealName,'' AS client_F,'' AS client_I, '' AS client_O,'' AS client_C,0 AS clientCommnon FROM crm.last_contact WHERE crm.last_contact.company_id = ? AND crm.last_contact.called_num LIKE ? LIMIT 1", [crm_id, num, crm_id, num]);
      conn.end();
      // console.log('super_stickiness_minutes ',rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- stickylong handle connection', err);
      return null;
    });
}

// SELECT crm.users.internal_phone, crm.users.realname AS manRealName, crm.customers.family_name AS client_F, crm.customers.name AS client_I, crm.customers.second_name AS client_O, crm.customers.company_name AS client_C, crm.customers.common_view_fl AS clientCommnon from crm.users, crm.customers,crm.customers_phones where crm.customers.owner_user_id=crm.users.id and crm.customers.rowstatus_id = '1' and crm.customers.id=crm.customers_phones.customer_id and crm.customers.company_id=? and crm.customers_phones.phone LIKE %? UNION SELECT crm.last_contact.mng_num AS internal_phone, '' AS manRealName,'' AS client_F,'' AS client_I, '' AS client_O,'' AS client_C,0 AS clientCommnon FROM crm.last_contact WHERE crm.last_contact.company_id = ? AND crm.last_contact.called_num LIKE %? LIMIT 1

exports.sticky = async (crm_id, num, max_d) => {
  //  console.log('all_users_params_by_integretions int_name ',int_name);
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.last_contact.mng_num as internal_phone FROM crm.last_contact WHERE crm.last_contact.company_id = ? AND crm.last_contact.ts > DATE_ADD(NOW(), INTERVAl -1 *? MINUTE) AND crm.last_contact.called_num LIKE ? UNION SELECT crm.users.internal_phone FROM crm.users ,crm.customers, crm.customers_phones WHERE crm.users.id=crm.customers.owner_user_id and crm.customers_phones.customer_id = crm.customers.id AND crm.customers.rowstatus_id = 1 AND crm.customers.company_id = ? AND crm.customers.common_view_fl <> 1 AND crm.customers_phones.phone LIKE ? LIMIT 1", [crm_id, max_d, num, crm_id, num]);
      conn.end();
      // console.log('super_stickiness_minutes ',rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- stickylong handle connection', err);
      return null;
    });
}
// crm_id, max_d, num, crm_id, num
// SELECT crm.last_contact.mng_num FROM crm.last_contact WHERE crm.last_contact.company_id = ? AND crm.last_contact.ts > DATE_ADD(NOW(), INTERVAl -1 *? MINUTE) AND crm.last_contact.called_num LIKE ? UNION SELECT crm.users.internal_phone FROM crm.users ,crm.customers, crm.customers_phones WHERE crm.users.id=crm.customers.owner_user_id and crm.customers_phones.customer_id = crm.customers.id AND crm.customers.rowstatus_id = 1 AND crm.customers.company_id = ? AND crm.customers.common_view_fl <> 1 AND crm.customers_phones.phone LIKE ? LIMIT 1

exports.conditionalRouting = async (crm_id) => {
  //  console.log('all_users_params_by_integretions int_name ',int_name);
  let log = bunyan.createLogger(bunyan_conf);
  return await mariadb.createConnection(options)
    .then(async conn => {
      let rows = await conn.query("SELECT crm.vats_modes.name from crm.companies, crm.vats_modes WHERE crm.companies.vats_mode_id = crm.vats_modes.id and crm.companies.id = ?", [crm_id]);
      conn.end();
      // console.log('super_stickiness_minutes ',rows);
      return rows;
    })
    .catch(err => {
      log.error(Date.now(), err);
      console.log('--------- to log ---------- streamtele_get_client_info_channels handle connection error');
      return null;
    });
}

// SELECT crm.vats_modes.name from crm.companies, crm.vats_modes WHERE crm.companies.vats_mode_id = crm.vats_modes.id and crm.companies.id = ?
