
const Ami = require('asterisk-manager');
const conf_ami_sip = require('../../../servconf').ami.sip;
const conf_ami_sipu = require('../../../servconf').ami.sipu;

exports.call_GenerateCall = async (req, res) => {
    console.log('data ',req.body);
    // req.body.name 
    // req.body.data
    // req.body.from
    // req.body.to
    // req.body.part  // sip,sipu

    if((req.body)&&(req.body.part==='sip')){

        let ami_sip = new Ami(conf_ami_sip.port, conf_ami_sip.host, conf_ami_sip.login, conf_ami_sip.pass, true);
        let act = {
            'action': 'GenerateCall',
            'account':(req.body.data)?req.body.data:null,
            'callerid':(req.body.name)? req.body.name+'<'+ req.body.from+'>':req.body.from,
            'channel': 'Local/' + req.body.from + '@crm_callback/n',
            'context': 'crm_callback',
            'priority': 1,
            'exten':'s',
            'variable': {'CALLBACK_ID':req.body.from,'DIAL_ID': req.body.to,'USERNAME':req.body.from}
        };
        console.log('act ',act);
        ami_sip.action(act, async (err, res) => {
            console.log('ami res ',res);
            ami_sip.disconnect();
             });
             res.status(200).send({ auth: true, message:"Originate successfully queued"});    
         //   return 'Originate successfully queued';
    
        } else if((req.body)&(req.body.part==='sipu')){
        let ami_sipu = new Ami(conf_ami_sipu.port, conf_ami_sipu.host, conf_ami_sipu.login, conf_ami_sipu.pass, true);
        let act = {
            'action': 'GenerateCall',
            'account':(req.body.data)?req.body.data:null,
            'callerid':(req.body.name)? req.body.name+'<'+ req.body.from+'>':req.body.from,
            'channel': 'Local/' + req.body.from + '@crm_callback/n',
            'context': 'crm_callback',
            'priority': 1,
            'exten':'s',
            'variable': {'CALLBACK_ID':req.body.from,'DIAL_ID': req.body.to,'USERNAME':req.body.from}
        };
        console.log('act ',act);
        ami_sip.action(act, async (err, res) => {
            console.log('ami res ',res);
            ami_sip.disconnect();
             });    
            return 'Originate successfully queued';
        }else{}
        
}
exports.call_GenerateCall_long = async (req, res) => {}
exports.call_Originate = async (req, res) => {}
exports.call_api_stream = async (req, res) => {}
exports.call_sip_stream = async (req, res) => {}
