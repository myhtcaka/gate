const request = require('request-promise');
const redis = require('./redis');
const dateFormat = require('dateformat');
const md5 = require('md5');
exports.send_custom = async (data) => {
    // console.log(data);
    //   { company_id: 2933,
    //     cdr_web_hook: 'https://pbxapi.prostiezvonki.ru/MDhUQTI3/|CLASSprostiezvonki|debug',
    //     nummass: [ '80044341201', '80044341202', '80044341203' ],
    //     event: 'CRMStart',
    //     privilege: 'call,all',
    //     accountcode: 'user:9419',
    //     channel: 'SIP/80044341203-000b7d9a',
    //     channelid: '1557989776.3476452',
    //     callid: '1557989776.3476448',
    //     sessionid: '1557989776.3476448',
    //     callerid: '380636414032',
    //     calleeid: '80044341203',
    //     realcalleeid: '80044341203',
    //     direction: 'originate',
    //     devicestate: 'RINGING',
    //     callback: '0',
    //     webphoneaccountcode: '',
    //     type: 'in',
    //     from: '380636414032',
    //     to: '80044341203',
    //     time: 1557989776581 }  
    // { company_id: 2933,
    //     cdr_web_hook: 'https://pbxapi.prostiezvonki.ru/MDhUQTI3/|CLASSprostiezvonki|debug',
    //     nummass: [ '80044341201', '80044341202', '80044341203' ],
    //     event: 'Answer',
    //     privilege: 'call,all',
    //     accountcode: 'user:9419',
    //     channel: 'SIP/80044341203-000b9554',
    //     channelstate: '6',
    //     channelstatedesc: 'Up',
    //     calleridnum: '380636414032',
    //     calleridname: '',
    //     connectedlinenum: '380636414032',
    //     connectedlinename: '',
    //     uniqueid: '1557990695.3512747',
    //     type: 'in',
    //     from: '380636414032',
    //     to: '80044341203',
    //     time: 1557990696484,
    //     sessionid: 'test',
    //     channelid: '1557990695.3512747' }
    // { company_id: 2933,
    //     cdr_web_hook: 'https://pbxapi.prostiezvonki.ru/MDhUQTI3/|CLASSprostiezvonki|debug',
    //     nummass: [ '80044341201', '80044341202', '80044341203' ],
    //     event: 'Hangup',
    //     privilege: 'call,all',
    //     channel: 'SIP/80044341203-000baf4b',
    //     uniqueid: '1557991777.3537513',
    //     calleridnum: '380636414032',
    //     calleridname: '<unknown>',
    //     connectedlinenum: '380636414032',
    //     connectedlinename: '<unknown>',
    //     accountcode: 'user:9419',
    //     cause: '16',
    //     'cause-txt': 'Normal clearing',
    //     initiator: 'System',
    //     type: 'in',
    //     from: '380636414032',
    //     to: '80044341203',
    //     time: 1557991787915,
    //     sessionid: 'test',
    //     channelid: '1557991777.3537513' }
//     status: 	ANSWER, – вызов отвечен
//          BUSY, – абонент занят
//          NOANSWER, – абонент не ответил после определённого таймаута
//          CANCEL, – вызов сброшен
//          CONGESTION, – перегрузка сети
//          CHANUNAVAIL – абонент недоступен (например sip абонент не зарегистрирован в сети)
//    call_start_timestamp – время начала вызова
//    call_answer_timestamp – время ответа на вызов, в случае отсутствия ответа, значение данного параметра должно быть равно 0.
//   
//    Дополнительные параметры:
//    -	call_record_link
    if(data.event=='CRMStart'){eve_send='1'; h='';}
    else if (data.event=='Answer'){eve_send='3'; h='';}
    else if (data.event=='Hangup'){
    //    console.log(data);
        eve_send='2';
        let time_ev = await redis.get_ev_times(data);
      //  console.log('time_ev ',time_ev);
        let t1 = dateFormat(Date.now().request_date, "yyyymmdd");
       // console.log(t1);
        let id ='2933';
        let user_group_id = '1241';
        //  md5($id.$user_group_id.$session_id); 
        

        let hash = md5(id+user_group_id+time_ev.sess);
       // console.log(hash);
        
        let mp3 = t1+'/'+time_ev.sess+'_'+hash;
        
        if (data.Answer_is=='1'){h='&status=ANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+(time_ev.call_answer_timestamp).slice(0,-3)+'&call_record_link=https://audio.streamtele.com/'+mp3;}
         else {h='&status=NOANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+time_ev.call_answer_timestamp;}
    }

    let send = 'event='+eve_send+'&call_id=' + data.channelid + '&src_type=1&dst_type=2&src_num=' + data.from + '&dst_num=' + data.to + '&timestamp=' + (data.time).toString().slice(0,-3);
    let URLs = (data.cdr_web_hook).split('|')[0] + '?' + send+h;
    console.log(URLs);

      const options = {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        uri: URLs
    };

    try {
        const response = await request(options);
        console.log(response);
        return '1';
    }
    catch (error) {
        console.log(error);
        return '0';
    }

}


