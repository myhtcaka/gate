const reqw = require('./services/doctor_eleks/reqwest');
const sql = require('./services/mariadb');

exports.integrations_data = async (req, res) => {
  // console.log('- data_all_integrations - ',req.body);
  //  let resp = await reqw.integrations_data();
  let data = await sql.get_integrations_doctorEleks();
  // console.log(resp);
  res.status(200).send({
    auth: true,
    data
  });
}

exports.streamtele_change_clients = async (req, res) => {
  // console.log('- data streamtele_change_clients - ',req.body);
  // { id: 434, from: '2019-07-11 10:42:28', to: '2019-07-11 11:59:29' }
  let data = await sql.streamtele_change_clients(req.body.id, req.body.from, req.body.to);
  //  let resp = await reqw.streamtele_change_clients(req.body.id,req.body.from,req.body.to);
  console.log(data);
  res.status(200).send({
    auth: true,
    data
  });
}

exports.streamtele_get_doctoreleks_id = async (req, res) => {
  let data = await sql.streamtele_get_doctoreleks_id(req.body.customer_id);
  //  let resp = await reqw.streamtele_get_doctoreleks_id(req.body.customer_id);
  console.log(data);
  res.status(200).send({
    auth: true,
    data
  });
}

exports.streamtele_get_clients_phones = async (req, res) => {
  let data = await sql.streamtele_get_clients_phones(req.body.customer_id);
  // let resp = await reqw.streamtele_get_clients_phones(req.body.customer_id);
  console.log(data);
  res.status(200).send({
    auth: true,
    data
  });
}

exports.streamtele_get_client_info_channels = async (req, res) => {
  let data = await sql.streamtele_get_client_info_channels(req.body.info_channels_id);
  //  let resp = await reqw.streamtele_get_client_info_channels(req.body.info_channels_id);
  console.log(data);
  res.status(200).send({
    auth: true,
    data
  });
}

exports.user_internal_by_uid = async (req, res) => {
  let data = await sql.user_internal_by_uid(req.body.insert_by_user_id);
  //  let resp = await reqw.streamtele_get_client_info_channels(req.body.info_channels_id);
  console.log(data);
  res.status(200).send({
    auth: true,
    data
  });
}