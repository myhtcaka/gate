const redis = require('../services/redis');
const key = require('../services/keys');
const dateFormat = require('dateformat');
const request = require('request-promise');

exports.retailcrm = async (all_data,conf_send) => {
//  console.log('--retailcrm--',all_data);
  if ((all_data.event == 'Track') && (all_data.type=='in') ) {
//  console.log( 'Track');
    await redis.save_key(all_data);
    }
    else if ((all_data.event == 'ClickToCall') && (all_data.type=='in') ) {
//    console.log( 'ClickToCall');
        await redis.save_key(all_data);
    }
  else if ((all_data.event == 'CRMStart') && (all_data.type=='in') ) {
//	console.log( 'CRMStart');
        await redis.save_key(all_data);
        let external_num = await redis.get_external_num(all_data);
//        console.log('external_num ',external_num);
        if (external_num){ console.log('external_num', external_num);
        let URLs = (conf_send.web_hook).split('|')[0] + '/api/v5/telephony/call/event?apiKey='+conf_send.client_key;
        let ev = '{ "phone": "'+all_data.from+'","type": "'+all_data.type+'","codes":['+all_data.to+'],"externalPhone": "'+external_num+'","callExternalId":"'+all_data.channelid+'"}';
                 const options = {
                    headers: { 'Content-Type': conf_send.content_type },
                    uri: URLs,
                    form: {
                        event: ev 
                         
                    }
                    };
               console.log(options);
        
            try {
                const response = await request.post(options);
                console.log(response);
                return '1';
            }
            catch (error) {   console.log('error CRMStart send');
                console.log(error);
                return '0';
            }

        }
        else{
            console.log('-------- no ext num ------------to log  ');
            
        }

    }  
  else if ((all_data.event == 'Answer') && (all_data.type=='in')){
    all_data.sessionid = 'test';
    all_data.channelid = all_data.uniqueid;
    await redis.save_key(all_data);
  }  

  else if ((all_data.event == 'Hangup') && (all_data.type=='in')) {
    all_data.sessionid = 'test';
    all_data.channelid = all_data.uniqueid;
    await redis.save_key(all_data);
    let is = await redis.check_hangup_a(all_data);
        if (is == 1) {
            console.log('--Hangup send--');
            all_data.Answer_is=1;
            this.retailcrm_send_hangup(all_data,conf_send);
         //   await reqw.send_custom(req.body)
        }
        else {
            all_data.Answer_is=0;
            let last = await redis.check_hangup_last(all_data);
            console.log('last is ', last);
            
            if (last == 1) {
                console.log('--Hangup no A send--');
                this.retailcrm_send_hangup(all_data,conf_send);
            }
            else {
                console.log('--Hangup no A break --');
                let ctc = await redis.check_clicktocall(all_data);
                console.log('clicktocall ',ctc);
                if(ctc){
                    console.log('send click to call ');
                    retailcrm_send_click(all_data,conf_send,ctc);
                    
                }
                
            }
        }
      //  res.status(200).send({ auth: true, token: 'accessToken' });
    }
// ---------------------------------------out ------------------------

 else if ((all_data.event == 'CRMStart') && (all_data.type=='out')) {
   //  console.log('OUT CRMStart ',all_data);
     await redis.save_key(all_data);
    }
    else if ((all_data.event == 'Answer') && (all_data.type=='out')) {
            all_data.sessionid = 'test';
            all_data.channelid = all_data.uniqueid;
            await redis.save_key(all_data);
    }
    else if ((all_data.event == 'Hangup') && (all_data.type=='out')) {
       // all_data.sessionid = 'test';
        all_data.channelid = all_data.uniqueid;
    //    console.log('Hangup out --- ',all_data);
        all_data.Answer_is = await redis.check_hangup_a(all_data);
        all_data.to = await redis.get_out_to(all_data);
        let time_ev = await redis.get_ev_times(all_data);
        let timeStart = dateFormat(Date(time_ev.call_start_timestamp), "yyyy-mm-dd HH:MM:ss");
       // console.log('time_ev ',time_ev);
       // let t1 = dateFormat(Date.now().request_date, "yyyymmdd");
       // console.log(t1);
        let mp3 = await key.get_audio_key2(time_ev.sess);
        //console.log('jwt ',key.audio(time_ev.sess));
      //  let is = await redis.check_hangup_a(all_data);
      let ev2 = '{ "phone": "'+all_data.to+'","type": "'+all_data.type+'","code":"'+all_data.from+'","callExternalId":"'+all_data.channelid+'","date":"'+timeStart+'","externalId":"'+all_data.channelid+'"';
      let URLs2 = (conf_send.web_hook).split('|')[0] + '/api/v5/telephony/calls/upload?apiKey='+conf_send.client_key;
    
      if (all_data.Answer_is=='1'){
            
            let duration =(all_data.time-time_ev.call_start_timestamp).toString().slice(0,-3);
            let durationWaiting=(time_ev.call_answer_timestamp-time_ev.call_start_timestamp).toString().slice(0,-3);
            let recordUrl='https://gate.streamtele.com/api/retailcrm/audio?'+mp3;
       
           const options2 = {
                   headers: { 'Content-Type': conf_send.content_type },
                   uri: URLs2,
                   form: { calls:'['+ev2+',"duration":"'+duration+'","durationWaiting":"'+durationWaiting+'","recordUrl":"'+recordUrl+'.mp3","result":"answered"}]' }
                   };
               await retailcrm_send_event(options2);

        }
        else {
            const options2 = {
                headers: { 'Content-Type': conf_send.content_type },
                uri: URLs2,
                form: { calls:'['+ev2+',"result":"no answer"}]' }
                };
        await retailcrm_send_event(options2);
        
        }
        


 //       console.log('--is- answ for hang-', is);
        }    

}

retailcrm_send_click = async (all_data,conf_send,time) => {
 // console.log(all_data);
//   calleridnum: '380636414032',
//   calleridname: '<unknown>',
//   connectedlinenum: '0443002160',
//   connectedlinename: '80044167201',
  
  let timeStart = dateFormat(Date(time.call_start_timestamp), "yyyy-mm-dd HH:MM:ss");
  let mp3 = await key.get_audio_key2(time.sess);
  let ev2 = '{ "phone": "'+all_data.from+'","type": "out","code":"'+all_data.to+'","callExternalId":"'+all_data.channelid+'","date":"'+timeStart+'","externalId":"'+all_data.channelid+'"';
  let URLs2 = (conf_send.web_hook).split('|')[0] + '/api/v5/telephony/calls/upload?apiKey='+conf_send.client_key;
  if(time.call_answer_timestamp==='0'){
    const options = {
        headers: { 'Content-Type': conf_send.content_type },
        uri: URLs2,
        form: { calls:'['+ev2+',"result":"no answer"}]' }
        };
        await retailcrm_send_upload(options);   

  }
  else {
    let duration =(all_data.time-time.call_start_timestamp).toString().slice(0,-3);
     let durationWaiting=(time.call_answer_timestamp-time.call_start_timestamp).toString().slice(0,-3);
     let recordUrl='https://gate.streamtele.com/api/retailcrm/audio?'+mp3;

     const options = {
        headers: { 'Content-Type': conf_send.content_type },
        uri: URLs2,
        form: { calls:'['+ev2+',"duration":"'+duration+'","durationWaiting":"'+durationWaiting+'","recordUrl":"'+recordUrl+'.mp3","result":"answered"}]' }
        };
        await retailcrm_send_upload(options); 
  }
}



// retailcrm

exports.retailcrm_send_hangup = async (all_data,conf_send) => {

    let time_ev = await redis.get_ev_times(all_data);
    let timeStart = dateFormat(Date(time_ev.call_start_timestamp), "yyyy-mm-dd HH:MM:ss");
    all_data.sessionid=all_data.uniqueid;
    let external_num = await redis.get_external_num(all_data);
    let mp3 = await key.get_audio_key2(time_ev.sess);
    let ctc = await redis.check_clicktocall(all_data);
   // console.log('mp3 ',mp3);
   if(external_num){
       console.log('is external_num ', external_num);
       console.log('clicktocall ',ctc);
   }
    let ev1 = '{ "phone": "'+all_data.from+'","type": "'+all_data.type+'","codes":['+all_data.to+'],"externalPhone": "'+external_num+'","callExternalId":"'+all_data.channelid+'","hangupStatus":"no answered"}';
    let ev2 = '{ "phone": "'+all_data.from+'","type": "'+all_data.type+'","code":"'+all_data.to+'","externalPhone": "'+external_num+'","callExternalId":"'+all_data.channelid+'","date":"'+timeStart+'","externalId":"'+all_data.channelid+'"';
    let URLs1 = (conf_send.web_hook).split('|')[0] + '/api/v5/telephony/call/event?apiKey='+conf_send.client_key;
    let URLs2 = (conf_send.web_hook).split('|')[0] + '/api/v5/telephony/calls/upload?apiKey='+conf_send.client_key;
  if (all_data.Answer_is=='1'){ 
  //    console.log('Answer_is ',all_data.Answer_is);
    //   { call_start_timestamp: '1559302189988',
    //   call_answer_timestamp: '1559302191069',
    //   sess: '1559302189.1819443' }
    // {"success":true}
     let duration =(all_data.time-time_ev.call_start_timestamp).toString().slice(0,-3);
     let durationWaiting=(time_ev.call_answer_timestamp-time_ev.call_start_timestamp).toString().slice(0,-3);
     let recordUrl='https://gate.streamtele.com/api/retailcrm/audio?'+mp3;



      const options1 = {
        headers: { 'Content-Type': conf_send.content_type },
        uri: URLs1,
        form: { event: ev1 }
        };
    const options2 = {
            headers: { 'Content-Type': conf_send.content_type },
            uri: URLs2,
            form: { calls:'['+ev2+',"duration":"'+duration+'","durationWaiting":"'+durationWaiting+'","recordUrl":"'+recordUrl+'.mp3","result":"answered"}]' }
            };
        await retailcrm_send_event(options1);
        await retailcrm_send_upload(options2);    

        }
    else {
        console.log('Answer_is ',all_data.Answer_is);
        const options1 = {
            headers: { 'Content-Type': conf_send.content_type },
            uri: URLs1,
            form: { event: ev1 }
            };
        const options2 = {
                headers: { 'Content-Type': conf_send.content_type },
                uri: URLs2,
                form: { calls:'['+ev2+',"result":"no answer"}]' }
                };
        await retailcrm_send_event(options1);
        await retailcrm_send_upload(options2);    

    }
    
    
    
        
//console.log(options2);


}

retailcrm_send_event = async (options) => {
    try {
        const response = await request.post(options);
        console.log(response);
        return '1';
    }
    catch (error) {
        console.log(error);
        return '0';
    }
}

retailcrm_send_upload = async (options) => {
    console.log(options);
    
    try {
        const response = await request.post(options);
        console.log(response);
        return '1';
    }
    catch (error) {
        console.log(error);
        return '0';
    }
}


// /api/v4/telephony/calls/upload

// {  t2 = dateFormat(t, "yyyy-mm-dd HH:MM:ss");
// 	date: {DateTime YYYY-MM-DD HH:MM:SS},
//  	"type": "in",
//     	"phone": "+75555555555",
//    	"codes":["80044167201"],
// 	result:answered|busy|failed|no answer|not allowed|unknown
// 	duration: {>=0, <=1000000000}}	Длительность звонка
// 	durationWaiting:{>=0, <=1000000000}}
// 	externalId: sess
// 	recordUrl:
//     	"externalPhone": "74444444444"


// }



//     if(all_data.event=='CRMStart'){eve_send='1'; h='';}
//     else if (all_data.event=='Answer'){eve_send='3'; h='';
//     all_data.sessionid = 'test';
//     all_data.channelid = all_data.uniqueid;
//     }
//     else if (all_data.event=='Hangup'){

//         all_data.sessionid = 'test';
//         all_data.channelid = all_data.uniqueid;
//         eve_send='2';
//         let time_ev = await redis.get_ev_times(all_data);
//       //  console.log('time_ev ',time_ev);
//         let t1 = dateFormat(Date.now().request_date, "yyyymmdd");
//        // console.log(t1);
//         let id ='2933';
//         let user_group_id = '1241';
//         //  md5($id.$user_group_id.$session_id); 
        

//         let hash = md5(id+user_group_id+time_ev.sess);
//        // console.log(hash);
        
//         let mp3 = t1+'/'+time_ev.sess+'_'+hash;
        
//         if (all_data.Answer_is=='1'){h='&status=ANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+(time_ev.call_answer_timestamp).slice(0,-3)+'&call_record_link=https://audio.streamtele.com/'+mp3;}
//          else {h='&status=NOANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+time_ev.call_answer_timestamp;}
//     }
//     await redis.save_key(all_data);

//     let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=1&dst_type=2&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
   
//     let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;

//     //  await redis.save_key(req.body);

//       const options = {
//         headers: { 'Content-Type': conf_send.content_type },
//         uri: URLs
//         };
//         console.log(options);
//         // console.log(URLs);   

//     // console.log('---------prostiezvonki---', all_data);
//     // console.log('conf send ',conf_send);
    


//     if ((all_data.event == 'CRMStart') && (all_data.type=='in') ) {
//         await redis.save_key(all_data);
//         eve_send='1'; h='';
//         let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=1&dst_type=2&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
//         let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;
//          const options = {
//             headers: { 'Content-Type': conf_send.content_type },
//             uri: URLs
//             };
//     //    console.log(options);

//     try {
//         const response = await request(options);
//         console.log(response);
//         return '1';
//     }
//     catch (error) {
//         console.log(error);
//         return '0';
//     }

//     //    console.log('CRMStart ',req.body);
//     //    await redis.save_key(req.body);
//     //    await reqw.send_custom(req.body);
//     //    res.status(200).send({ auth: true, token: 'accessToken' });
//     }
//     else if ((all_data.event == 'Answer') && (all_data.type=='in')) {
//      //   console.log('in ans ',all_data);
        
//         eve_send='3'; h='';
//     all_data.sessionid = 'test';
//     all_data.channelid = all_data.uniqueid;
//         await redis.save_key(all_data);
//         let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=1&dst_type=2&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
//         let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;
//          const options = {
//             headers: { 'Content-Type': conf_send.content_type },
//             uri: URLs
//             };
//        // console.log(options);

//         try {
//             const response = await request(options);
//             console.log(response);
//             return '1';
//         }
//         catch (error) {
//             console.log(error);
//             return '0';
//         }
//      //    console.log('Answer ',req.body);
//         //
//      //   req.body.sessionid = 'test';
//      //   req.body.channelid = req.body.uniqueid;
//      //   console.log(await redis.save_key(req.body));
//      //   await reqw.send_custom(req.body);
//      //   res.status(200).send({ auth: true, token: 'accessToken' });
//     }
//     else if ((all_data.event == 'Hangup') && (all_data.type=='in')) {
//         all_data.sessionid = 'test';
//         all_data.channelid = all_data.uniqueid;
//             await redis.save_key(all_data);
//         let is = await redis.check_hangup_a(all_data);
//         console.log('--is- answ for hang-', is);

//         if (is == 1) {
//             console.log('--Hangup send--');
//             all_data.Answer_is=1;
//             this.prostiezvonki_send_hangup(all_data,conf_send);
//          //   await reqw.send_custom(req.body)
//         }
//         else {
//             all_data.Answer_is=0;
//             let last = await redis.check_hangup_last(all_data);
//             console.log('last is ', last);
            
//             if (last == 1) {
//                 console.log('--Hangup no A send--');
//                 this.prostiezvonki_send_hangup(all_data,conf_send);
//             }
//             else {
//                 console.log('--Hangup no A break --');
//             }
//         }
//       //  res.status(200).send({ auth: true, token: 'accessToken' });
//     }


// // ---------------------------------------out ------------------------

//  else if ((all_data.event == 'CRMStart') && (all_data.type=='out')) {
//     await redis.save_key(all_data);
//     eve_send='1'; h='';
//     let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=2&dst_type=1&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
//     let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;
//      const options = {
//         headers: { 'Content-Type': conf_send.content_type },
//         uri: URLs
//         };
//     console.log(options);

// try {
//     const response = await request(options);
//     console.log(response);
//     return '1';
// }
// catch (error) {
//     console.log(error);
//     return '0';
// }
// // console.log('CRMStart out --- ',all_data);
// }
// else if ((all_data.event == 'Answer') && (all_data.type=='out')) {
//     all_data.sessionid = 'test';
//     all_data.channelid = all_data.uniqueid;
//     await redis.save_key(all_data);
//     all_data.to = await redis.get_out_to(all_data);
//     eve_send='3'; h='';
//     let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=2&dst_type=1&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
//     let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;
//      const options = {
//         headers: { 'Content-Type': conf_send.content_type },
//         uri: URLs
//         };
//     console.log(options);

 
 
//     //   console.log('Answer out --- ',all_data);
//     }
// else if ((all_data.event == 'Hangup') && (all_data.type=='out')) {
//        // all_data.sessionid = 'test';
//         all_data.channelid = all_data.uniqueid;
//     //    console.log('Hangup out --- ',all_data);
//         all_data.Answer_is = await redis.check_hangup_a(all_data);
//         all_data.to = await redis.get_out_to(all_data);
//         eve_send='2';
//         let time_ev = await redis.get_ev_times(all_data);
//        // console.log('time_ev ',time_ev);
//        // let t1 = dateFormat(Date.now().request_date, "yyyymmdd");
//        // console.log(t1);
//         let mp3 = await key.get_audio_key2(time_ev.sess);
//         //console.log('jwt ',key.audio(time_ev.sess));
        
//       //  let is = await redis.check_hangup_a(all_data);
           
//         if (all_data.Answer_is=='1'){h='&status=ANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+(time_ev.call_answer_timestamp).slice(0,-3)+'&call_record_link=https://gate.streamtele.com/api/prostiezvonki/audio?'+mp3;}
//         else {h='&status=NOANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+time_ev.call_answer_timestamp;}
//         let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=1&dst_type=2&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
//         let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;
//         const options = {
//             headers: { 'Content-Type': conf_send.content_type },
//             uri: URLs
//             };
//         console.log(options);



//  //       console.log('--is- answ for hang-', is);
//         }    

// }


// exports.prostiezvonki_send_hangup = async (all_data,conf_send) => {

//     eve_send='2';
//     let time_ev = await redis.get_ev_times(all_data);
//     // console.log('time_ev ',time_ev);
//    // let t1 = dateFormat(Date.now().request_date, "yyyymmdd");
//    // console.log(t1);
//    // let mp3 = t1+'/'+time_ev.sess;
//    let mp3 = await key.get_audio_key2(time_ev.sess);
//   //  console.log('jwt ',await key.get_audio_key(time_ev.sess));
//     if (all_data.Answer_is=='1'){h='&status=ANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+(time_ev.call_answer_timestamp).slice(0,-3)+'&call_record_link=https://gate.streamtele.com/api/prostiezvonki/audio?'+mp3;}
//     else {h='&status=NOANSWER&call_start_timestamp='+(time_ev.call_start_timestamp).slice(0,-3)+'&call_answer_timestamp='+time_ev.call_answer_timestamp;}
//     let send = 'event='+eve_send+'&call_id=' + all_data.channelid + '&src_type=1&dst_type=2&src_num=' + all_data.from + '&dst_num=' + all_data.to + '&timestamp=' + (all_data.time).toString().slice(0,-3);
//     let URLs = (conf_send.web_hook).split('|')[0] + '?' + send+h;
//     const options = {
//         headers: { 'Content-Type': conf_send.content_type },
//         uri: URLs
//         };
//     console.log(options);

//     try {
//         const response = await request(options);
//         console.log(response);
//         return '1';
//     }
//     catch (error) {
//         console.log(error);
//         return '0';
//     }

// }