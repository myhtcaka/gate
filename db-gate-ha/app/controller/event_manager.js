const reqw = require('./services/event_manager/reqwest');
const sql = require('./services/mariadb');



exports.integrations_data = async (req, res) => {
  // console.log('- data_all_integrations - ',req.body);
  let data = await sql.get_integrations();
  //   let resp = await reqw.integrations_data();
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}
