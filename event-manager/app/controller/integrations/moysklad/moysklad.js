const redis = require('./redis');
const key = require('./keys');
const dateFormat = require('dateformat');
const request = require('request-promise');

exports.moysklad = async (all_data) => {
    // console.log('-all_data--- ',all_data);
    // ---------------------------------------in ------------------------
    if ((all_data.event == 'Track') && (all_data.type == 'in')) {
        //  console.log( 'moysklad Track in');
        await redis.save_key(all_data);
    }
    else if ((all_data.event == 'ClickToCall') && (all_data.type == 'in')) {
        console.log('ClickToCall ', all_data);

        //    all_data.type='out'
        await redis.save_key(all_data);
        //   let body={ "event":"StartCall", "type": "out","call_id":all_data.channelid,"event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to,"additionally":"clicktocall"};
        //   console.log('ClickToCall send StartCall ', await this.streamtele_send(body,conf_send));

    }

    else if ((all_data.event == 'CRMStart') && (all_data.type == 'in')) {
        await redis.save_key(all_data);
        let param_t = await redis.get_key_param('Track', all_data);
        if (param_t) {
            let body = {
                "externalId": all_data.channelid,
                "number": all_data.from,
                "extension": (all_data.to).slice(-8),
                "isIncoming": true,
                "startTime": dateFormat(new Date(all_data.time), "yyyy-mm-dd HH:MM:ss"),
                "events": [{ "eventType": "SHOW", "extension": (all_data.to).slice(-8), "sequence": 1 }]
            };
            let options = {
                "url": all_data.int.web_hook + '/call',
                "headers":
                {
                    "Lognex-Phone-Auth-Token": all_data.int.client_key,
                    "Content-Type": all_data.int.content_type
                },
                body,
                "method": "POST",
                "json": true

            };
            console.log('StartCall in moysklad_send ', await this.moysklad_send(options));
        }
        else {
            console.log('--no track (out call)--');

        }
    }



    else if ((all_data.event == 'Answer') && (all_data.type == 'in')) {
        await redis.save_key(all_data);

        let param_сс = await redis.get_key_param('ClickToCall', all_data);
        if (!param_сс) {
            let body = {
                "events": [{ "eventType": "STARTTIME", "extension": (all_data.to).slice(-8), "sequence": 2 }]
            };
            let options = {
                "url": all_data.int.web_hook + '/call/extid/' + all_data.uniqueid,
                "headers":
                {
                    "Lognex-Phone-Auth-Token": all_data.int.client_key,
                    "Content-Type": all_data.int.content_type
                },
                body,
                "method": "PUT",
                "json": true
            };
            console.log('Answer moysklad_send ', await this.moysklad_send(options));
        }
        else {
            console.log(' ClickToCall  Answer ', all_data);
        }
    }

    else if ((all_data.event == 'Hangup') && (all_data.type == 'in')) {
        await redis.save_key(all_data);
        let param_a = await redis.get_key_param('Answer', all_data);
        if (param_a) {
            let param_сс = await redis.get_key_param('ClickToCall', all_data);
            //console.log('param_сс ',param_сс);

            if (!param_сс) {


                let param_c = await redis.get_key_param('CRMStart', all_data);
                console.log('Hangup C param_ ', param_c);
                //     console.log('Hangup A param_ ', param_a);
                // console.log(new Date(param_a.time));
                //  console.log(new Date(all_data.time));
                let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(param_c.sessionid);
                //   console.log('Hangup A mp3 ',mp3);

                let body = {
                    "isIncoming": true,
                    "startTime": dateFormat(new Date(parseInt(param_a.time)), "yyyy-mm-dd HH:MM:ss"),
                    "endTime": dateFormat(new Date(all_data.time), "yyyy-mm-dd HH:MM:ss"),
                    "duration": all_data.time - param_a.time,
                    "recordUrl": [mp3],
                    "events": [{ "eventType": "HIDE", "extension": (all_data.to).slice(-8), "sequence": 3 }]
                }
                let options = {
                    "url": all_data.int.web_hook + '/call/extid/' + all_data.uniqueid,
                    "headers":
                    {
                        "Lognex-Phone-Auth-Token": all_data.int.client_key,
                        "Content-Type": all_data.int.content_type
                    },
                    body,
                    "method": "PUT",
                    "json": true

                };
                console.log('Hangup Answer moysklad_send ', await this.moysklad_send(options));
            }
            else {
                console.log('param_сс ', param_сс.sessionid);
                let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(param_сс.sessionid);
                let body = {
                    "externalId": param_сс.sessionid,
                    "number": all_data.from,
                    "extension": (all_data.to).slice(-8),
                    "isIncoming": false,
                    "startTime": dateFormat(new Date(parseInt(param_a.time)), "yyyy-mm-dd HH:MM:ss"),
                    "endTime": dateFormat(new Date(all_data.time), "yyyy-mm-dd HH:MM:ss"),
                    "duration": all_data.time - param_a.time,
                    "recordUrl": [mp3],

                }
                let options = {
                    "url": all_data.int.web_hook + '/call',
                    "headers":
                    {
                        "Lognex-Phone-Auth-Token": all_data.int.client_key,
                        "Content-Type": all_data.int.content_type
                    },
                    body,
                    "method": "POST",
                    "json": true

                };
                console.log('Hangup ClickToCall in moysklad_send ', await this.moysklad_send(options));
                //  console.log('Hangup ClickToCall  Answer ',all_data);
            }

        }



        else {
            console.log('Hangup no A all_data', all_data);
            let body = {
                "events": [{ "eventType": "HIDE", "extension": (all_data.to).slice(-8), "sequence": 2 }]
            };
            let options = {
                "url": all_data.int.web_hook + '/call/extid/' + all_data.uniqueid,
                "headers":
                {
                    "Lognex-Phone-Auth-Token": all_data.int.client_key,
                    "Content-Type": all_data.int.content_type
                },
                body,
                "method": "PUT",
                "json": true
            };
            console.log('Answer moysklad_send ', await this.moysklad_send(options));
        }
    }

    // ---------------------------------------out ------------------------
    else if ((all_data.event == 'CRMStart') && (all_data.type == 'out')) {
        //    console.log( 'CRMStart out moysklad ',all_data);
        await redis.save_key(all_data);
        let body = {
            "externalId": all_data.channelid,
            "number": all_data.to,
            "extension": (all_data.from).slice(-8),
            "isIncoming": false,
            "startTime": dateFormat(new Date(all_data.time), "yyyy-mm-dd HH:MM:ss")
        };
        let options = {
            "url": all_data.int.web_hook + '/call',
            "headers":
            {
                "Lognex-Phone-Auth-Token": all_data.int.client_key,
                "Content-Type": all_data.int.content_type
            },
            body,
            "method": "POST",
            "json": true
        };
        console.log('StartCall out moysklad_send ', await this.moysklad_send(options));
    }

    else if ((all_data.event == 'Answer') && (all_data.type == 'out')) {
        await redis.save_key(all_data);
        console.log('---out Answer --');
    }

    else if ((all_data.event == 'Hangup') && (all_data.type == 'out')) {
        await redis.save_key(all_data);
        let param_a = await redis.get_key_param('Answer', all_data);
        if (param_a) {
            //  let param_c = await redis.get_key_param('CRMStart', all_data);
            //   console.log('-- send out Hangup A ', param_a);
            //    console.log('-- send out Hangup C ', param_c);
            //    console.log('out Hangup A ', all_data);
            let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(all_data.uniqueid);
            let body = {

                "isIncoming": false,
                "startTime": dateFormat(new Date(parseInt(param_a.time)), "yyyy-mm-dd HH:MM:ss"),
                "endTime": dateFormat(new Date(all_data.time), "yyyy-mm-dd HH:MM:ss"),
                "duration": all_data.time - param_a.time,
                "recordUrl": [mp3]
            }
            let options = {
                "url": all_data.int.web_hook + '/call/extid/' + all_data.uniqueid,
                "headers":
                {
                    "Lognex-Phone-Auth-Token": all_data.int.client_key,
                    "Content-Type": all_data.int.content_type
                },
                body,
                "method": "PUT",
                "json": true
            };
            console.log('Hangup out moysklad_send ', await this.moysklad_send(options));
        }
        else {
            console.log('-- out Hangup no A ');
        }
    }
}

exports.moysklad_send = async (options) => {
    //   console.log(' --- moysklad_send options-- ',options.body);
    // console.log(' --- moysklad_send options .events-- ', options.body.events);
    try {
        const response = await request(options);
        //    console.log(response);
        return '1';
    }
    catch (error) {
        console.log(error);
        return null;
    }
}

