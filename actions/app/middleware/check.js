const jwt = require('jsonwebtoken');
const fs = require('fs');
const pub_keys = require('../../servconf').jwt.rsa_clients;

//const rsa_pub = fs.readFileSync(require('../../servconf').jwt);



authentication = (req, res, next) => {
    if (req.headers.authorization) {
        let token = req.headers.authorization.split(' ')[1];
        let pub_key = pub_keys.find(el => el.key === req.baseUrl.slice(1));
        if (!token || pub_key === undefined) { return res.status(403).send({ auth: false, message: 'No token provided. ' }); }

        jwt.verify(token, fs.readFileSync(pub_key.way), function (err, decoded) {
            if (err) { return res.status(403).send({ auth: false, message: 'No token provided.' }); }
            else {
                req.token = token;
                next();
            }
        });
    }
    else { return res.status(403).send({ auth: false, message: 'No token provided.' }); }
}



const check = {};
check.authentication = authentication;
// check.add_keys = add_keys;

module.exports = check;