const redis = require('./redis');

authentication = async (req, res, next) => {

 //   console.log('-check authentication-');
    next();
}

save_keys_ami_sip_v2 = async(req, res, next) => {
    
    if(req.body.int){
        await redis.save_keys_sip(req.body);
    //    console.log('- save_keys_ami_sip_v2 integrations -');
    }else{
        await redis.save_keys_start_in_sip(req.body);
    //    console.log('- save_keys_ami_sip_v2 no integrations start_in -');
    }
    // if integration save all
    // else save start in
    //   console.log('-save_keys_ami_sip_v2-');
       next();
   }

popup_ami_sip_v2 =async (req, res, next) => {
    if(req.body.a_marker==='ring'){
        // get_key  = async (ev,part,key_c,key_s)
     //  console.log('-=- popup_ami_sip ring to rabbit ',await redis.get_key(req.body,'start_in',req.body.sessionid,req.body.sessionid));
        

    }
    // для уменьшения нагрузки на реббит
     // if ring check key start in 
            // true check in SELECT crm.users.internal_phone from crm.users where  crm.users.show_call_popup=1 and crm.users.internal_phone
                // true send to rabbit    
    
   // console.log('-popup_ami_sip_v2-');
    next();
}

set_integration = async (req, res, next) => {

    let arrFound = await (req.body.int).filter((item) => { if (item.company_id === req.body.company_id) { return item; } });

   // console.log('-=- filtr ',arrFound);
    
    if (arrFound.length != 0) {
    //    console.log('-=- in filtr ');
        
       await arrFound.forEach(el1 => {
        req.body.int = el1;
       });
    } else {
        req.body.int=null;
    }
 //  console.log('-set_integration-');
    next();
}

      

const check = {};
check.authentication = authentication;
check.popup_ami_sip_v2 = popup_ami_sip_v2;
check.save_keys_ami_sip_v2= save_keys_ami_sip_v2;
check.set_integration = set_integration;
// check.add_keys = add_keys;

module.exports = check;