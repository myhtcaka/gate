const redis = require('../services/redis');
const key = require('../services/keys');
const dateFormat = require('dateformat');
const request = require('request-promise');



exports.istat24 = async (all_data,conf_send) => {
//  console.log('--istat24--',all_data.event,conf_send);

// ---------------------------------------in ------------------------
  if ((all_data.event == 'Track') && (all_data.type=='in') ) {
    //  console.log( 'Track');
    await redis.save_key(all_data);
    }
    else if ((all_data.event == 'ClickToCall') && (all_data.type=='in') ) {
        await redis.save_key(all_data);
        let body={ "event":"StartCall","sessionid":all_data.channelid ,"type": "out","event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to,"additionally":"clicktocall"};
        console.log('ClickToCall send StartCall ', await this.istat24_send(body,conf_send));
        
    }
    else if ((all_data.event == 'CRMStart') && (all_data.type=='in') ) {
   //	console.log( 'CRMStart');
        await redis.save_key(all_data);
        let external_num = await redis.get_external_num(all_data);
     //        console.log('external_num ',external_num);
    if (external_num){ console.log('external_num', external_num);
        let body={ "event":"StartCall","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to,"via":external_num};
      
        console.log('send StartCall ', await this.istat24_send(body,conf_send));   
    }
    else{
            console.log('----CRMStart---- no ext num ------------to log  ');
            
        }

    }  
    else if ((all_data.event == 'Answer') && (all_data.type=='in')) {
    //    console.log('Answer ',all_data);
        all_data.sessionid = 'Answer';
        all_data.channelid = all_data.uniqueid;
        await redis.save_key(all_data);
        let cc = await redis.check_clicktocall(all_data);
        if(!cc){
        
        
        let external_num = await redis.get_external_num(all_data);
        if (external_num){ console.log('external_num', external_num);
        let time_start = await redis.get_time_C(all_data);
    //    console.log('time_start',time_start);
        
        let body={"event":"Answer","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"time_start": time_start,"from": all_data.from,"to": all_data.to,"via":external_num};
              console.log('send Answer ', await this.istat24_send(body,conf_send));
        }
        else{console.log('-------- no ext num ------------to log  ');}
        }
        else{
            // console.log(' ClickToCall  Answer ',all_data);
            let body={"event":"Answer","sessionid":all_data.channelid, "type": "out","event_time": all_data.time.toString(),"time_start": cc.call_start_timestamp,"from": all_data.to,"to": all_data.from,"additionally":"clicktocall"};
            console.log('ClickToCall send Answer ', await this.istat24_send(body,conf_send));
        }
        
    }
    else if ((all_data.event == 'Hangup') && (all_data.type=='in')) {
        all_data.sessionid = 'Hangup';
        all_data.channelid = all_data.uniqueid;
        await redis.save_key(all_data);
        let is = await redis.check_hangup_a(all_data);
        console.log('--is- answ for hang-', is);
        if (is == 1) {
            let external_num = await redis.get_external_num(all_data);
                if (external_num){ console.log('external_num', external_num);
                let time_ev = await redis.get_ev_times(all_data);
                // console.log('time_ev ',time_ev);
                let mp3 = 'https://gate.streamtele.com/api/istat24/audio?'+await key.get_audio_key2(time_ev.sess);
                let body={"event":"Hangup","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"time_start": time_ev.call_start_timestamp,"time_answer": time_ev.call_answer_timestamp ,"from": all_data.from,"to": all_data.to,"via":external_num,"recordUrl":mp3,"result":"answer"};
                console.log('send Hangup ', await this.istat24_send(body,conf_send));
                }
                else{console.log('----Hangup is A---- no ext num ------------to log  ');}
        }
        else {
            all_data.Answer_is=0;
            let last = await redis.check_hangup_last(all_data);
            console.log('last is ', last);
            
            if (last == 1) {
                console.log('--Hangup no A send--');
                let external_num = await redis.get_external_num(all_data);
                if (external_num){ console.log('external_num', external_num);
                let time_start = await redis.get_time_C(all_data);
                let body={"event":"Hangup","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"time_start": time_start,"from": all_data.from,"to": all_data.to,"via":external_num,"recordUrl":"","time_answer": "","result":"no answer"};
                // console.log('body ',body);
                console.log('send Hangup ', await this.istat24_send(body,conf_send));
                }
                else{console.log('----Hangup no A---- no ext num ------------to log  ');}
            }
            else {
                let cc = await redis.check_clicktocall(all_data);
                if(cc){
                    //console.log(' ClickToCall Hangup ',cc);
                    if(cc.call_answer_timestamp!='0'){
                        let mp3 = 'https://gate.streamtele.com/api/istat24/audio?'+await key.get_audio_key2(cc.sess);
                        let body={"event":"Hangup","sessionid":all_data.channelid, "type": "out","event_time": all_data.time.toString(),"time_start": cc.call_start_timestamp,"time_answer": cc.call_answer_timestamp,"from": all_data.to ,"to": all_data.from,"recordUrl":mp3,"result":"answer","additionally":"clicktocall"};    
                        console.log('ClickToCall send Hangup A', await this.istat24_send(body,conf_send));
                    }
                    else{
                    let body={"event":"Hangup","sessionid":all_data.channelid, "type": "out","event_time": all_data.time.toString(),"time_start": cc.call_start_timestamp,"time_answer": "","from": all_data.to ,"to": all_data.from,"recordUrl":"","result":"no answer","additionally":"clicktocall"};
                    console.log('ClickToCall send Hangup no A ', await this.istat24_send(body,conf_send));
                    }

                }
                else{console.log('--Hangup no A break --');}
                
            }
        }
      }    
// ---------------------------------------out ------------------------
    else if ((all_data.event == 'CRMStart') && (all_data.type=='out')) {
        await redis.save_key(all_data);
        let body={ "event":"StartCall", "type": all_data.type,"event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to};
        console.log('send StartCall ', await this.istat24_send(body,conf_send));
    }

    else if ((all_data.event == 'Answer') && (all_data.type=='out')) {
        all_data.sessionid = 'Answer';
        all_data.channelid = all_data.uniqueid;
        await redis.save_key(all_data);
        let time_start = await redis.get_time_C(all_data);
        all_data.to = await redis.get_out_to(all_data);
        let body={"event":"Answer","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"time_start": time_start,"from": all_data.from,"to": all_data.to};
        console.log('send Answer ', await this.istat24_send(body,conf_send));
        }

    else if ((all_data.event == 'Hangup') && (all_data.type=='out')) {
        all_data.channelid = all_data.uniqueid;
        let is = await redis.check_hangup_a(all_data);
        all_data.to = await redis.get_out_to(all_data);
        let time_ev = await redis.get_ev_times(all_data);
        console.log('out Hangup answer is ',is);
        if (is == 1) {
            let mp3 = 'https://gate.streamtele.com/api/istat24/audio?'+await key.get_audio_key2(time_ev.sess);
            let body={"event":"Hangup","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"time_start": time_ev.call_start_timestamp,"time_answer": time_ev.call_answer_timestamp ,"from": all_data.from,"to": all_data.to,"recordUrl":mp3,"result":"answer"};
            console.log('send Hangup ', await this.istat24_send(body,conf_send));

        }
        else {
            let body={"event":"Hangup","sessionid":all_data.channelid, "type": all_data.type,"event_time": all_data.time.toString(),"time_start": time_ev.call_start_timestamp,"from": all_data.from,"to": all_data.to,"recordUrl":"","time_answer": "","result":"no answer"};
            console.log('send Answer ', await this.istat24_send(body,conf_send));  
        }
        
    }    

 }



exports.istat24_send = async (body,conf_send) => {

    let options = {
        headers: { 
         //   Accept: 'application/json',
            'Content-Type': conf_send.content_type
                },
        uri: conf_send.web_hook,
        method: 'POST',
        body,   
        json: true
        };
     //   console.log('options send ',options);
        
    try {
        const response = await request(options);
    //    console.log(response);
    return '1';
    }
    catch (error) {
        console.log(error);
    return null;
    }

}


