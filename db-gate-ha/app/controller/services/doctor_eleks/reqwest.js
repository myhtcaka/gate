
const request = require('request-promise');
const keys = require('../keys');
const urls = require('../../../../servconf').urls;


exports.integrations_data = async () => {
  ///integrations_doctorEleks
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/integrations_doctorEleks',
    json: true,
    body: {},
    method: 'POST',
    //  rejectUnauthorized: false,
  };
  //  console.log('options ',options);
  try {
    const response = await request(options);
    //    console.log('response ',response);
    console.log('send ok');
    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}
  }
  catch (error) {
    console.log('error  integrations_data ', error);
    console.log('doktor send error ');
    return '0';
  }
}

exports.streamtele_change_clients = async (id, from, to) => {
  ///integrations_doctorEleks
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/clients/streamtele_change_clients',
    json: true,
    body: { id, from, to },
    method: 'POST',
    //  rejectUnauthorized: false,
  };
  //  console.log('options ',options);
  try {
    const response = await request(options);
    //    console.log('response ',response);
    console.log('send ok');
    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}
  }
  catch (error) {
    console.log('error  integrations_data ', error);
    console.log('doktor send error ');
    return '0';
  }
}


exports.streamtele_get_doctoreleks_id = async (customer_id) => {
  ///integrations_doctorEleks streamtele_get_doctoreleks_id
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/clients/streamtele_get_doctoreleks_id',
    json: true,
    body: { customer_id },
    method: 'POST',
    //  rejectUnauthorized: false,
  };
  //  console.log('options ',options);
  try {
    const response = await request(options);
    //    console.log('response ',response);
    console.log('send ok');
    return response;
  }
  catch (error) {
    //console.log('error  streamtele_get_doctoreleks_id ', error);
    console.log('doktor streamtele_get_doctoreleks_id send error ');
    return '0';
  }
}

exports.streamtele_get_clients_phones = async (customer_id) => {
  ///integrations_doctorEleks streamtele_get_doctoreleks_id
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/clients/streamtele_get_clients_phones',
    json: true,
    body: { customer_id },
    method: 'POST',
    //  rejectUnauthorized: false,
  };
  //  console.log('options ',options);
  try {
    const response = await request(options);
    //    console.log('response ',response);
    console.log('send ok');
    return response;
  }
  catch (error) {
    //console.log('doktor error  streamtele_get_clients_phones ', error);
    console.log('doktor streamtele_get_clients_phones send error ');
    return '0';
  }
}

exports.streamtele_get_client_info_channels = async (info_channels_id) => {
  ///integrations_doctorEleks streamtele_get_doctoreleks_id
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/clients/streamtele_get_client_info_channels',
    json: true,
    body: { info_channels_id },
    method: 'POST',
    //  rejectUnauthorized: false,
  };
  //  console.log('options ',options);
  try {
    const response = await request(options);
    //    console.log('response ',response);
    console.log('send ok');
    return response;
  }
  catch (error) {
    //console.log('doktor error  streamtele_get_clients_phones ', error);
    console.log('doktor streamtele_get_clients_phones send error ');
    return '0';
  }
}


// /clients/streamtele_get_doctoreleks_id (customer_id)
// /clients/streamtele_get_clients_phones (customer_id)
// /clients/streamtele_get_client_info_channels (info_channels_id)
