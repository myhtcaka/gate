const redis = require("redis");
const rconf = require('../../../../servconf').redis;

const { promisify } = require('util');
client = redis.createClient(rconf.port, rconf.host);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);
const expireAsync = promisify(client.expire).bind(client);

// event: 'Answer',
//   privilege: 'call,all',
//   accountcode: 'user:9437',
//   channel: 'SIP/80044340202-00027338',
//   channelstate: '6',
//   channelstatedesc: 'Up',
//   calleridnum: '380636414032',
//   calleridname: '',
//   connectedlinenum: '380636414032',
//   connectedlinename: '',
//   uniqueid: '1567427032.371516',
//   company_id: 2942,
//   group_id: 1247,
//   type: 'in',
//   from: '380636414032',
//   to: '80044340202',
//   time: 1567427036629,

// event: 'CRMStart',
// privilege: 'call,all',
// accountcode: 'user:9437',
// channel: 'SIP/80044340202-0001bcaa',
// channelid: '1567419756.277565',
// callid: '1567419756.277562',
// sessionid: '1567419756.277562',
// callerid: '+380636414032',
// calleeid: '80044340202',
// realcalleeid: '80044340202',
// direction: 'originate',
// devicestate: 'RINGING',
// callback: '0',
// webphoneaccountcode: '',
// company_id: 2942,
// group_id: 1247,
// type: 'in',
// from: '+380636414032',
// to: '80044340202',
// time: 1567419756609,

// event: 'Track',
// privilege: 'call,all',
// accountcode: 'gateway:2698',
// channel: 'SIP/0443002000-000186fe',
// channelid: '1567417667.247926',
// callid: '1567417667.247926',
// sessionid: '1567417667.247926',
// callerid: '380636414032',
// calleeid: '380443002067',
// realcalleeid: '380443002067',
// direction: 'answer',
// devicestate: 'UNKNOWN',
// callback: '0',
// webphoneaccountcode: '',
// company_id: 2942,
// group_id: 1247,
// type: 'in',
// external_num: '380443002067',
// from: '380636414032',
// time: 1567417667902,

// Hangup A param  {
//     from: '380636414032',
//     to: '80044340201',
//     time: '1567435084428',
//     sessionid: '1567435084.464354'
//   }
//   Hangup A all_data {
//     event: 'Hangup',
//     privilege: 'call,all',
//     channel: 'SIP/80044340201-00032c13',
//     uniqueid: '1567435084.464357',
//     calleridnum: '380636414032',
//     calleridname: '<unknown>',
//     connectedlinenum: '380636414032',
//     connectedlinename: '<unknown>',
//     accountcode: 'user:9436',
//     cause: '16',
//     'cause-txt': 'Normal clearing',
//     initiator: 'System',
//     company_id: 2942,
//     group_id: 1247,
//     type: 'in',
//     from: '380636414032',
//     to: '80044340201',
//     time: 1567435090809,
//     int: {
//       name: 'moysklad',
//       company_id: 2942,
//       content_type: 'application/json',
//       web_hook: 'https://online.moysklad.ru/api/phone/1.0',
//       client_key: '6f2a63d1ea9d8d1b283cb13edd4b8eb3278b9354'
//     }

//   Hangup no A all_data {
//     event: 'Hangup',
//     privilege: 'call,all',
//     channel: 'SIP/80044340202-00032c14',
//     uniqueid: '1567435084.464358',
//     calleridnum: '380636414032',
//     calleridname: '<unknown>',
//     connectedlinenum: '380636414032',
//     connectedlinename: '<unknown>',
//     accountcode: 'user:9437',
//     cause: '26',
//     'cause-txt': 'Answered elsewhere',
//     initiator: 'System',
//     company_id: 2942,
//     group_id: 1247,
//     type: 'in',
//     from: '380636414032',
//     to: '80044340202',
//     time: 1567435087518,
//     int: {
//       name: 'moysklad',
//       company_id: 2942,
//       content_type: 'application/json',
//       web_hook: 'https://online.moysklad.ru/api/phone/1.0',
//       client_key: '6f2a63d1ea9d8d1b283cb13edd4b8eb3278b9354'
//     }

exports.save_key = async (data) => {
    //    console.log(data);
    let key = 'eve_m:' + data.company_id + ':' + data.type + ':' + data.event + ':' + (await (data.channelid === undefined) ? data.uniqueid : data.channelid);
    let val = data.from + ',' + (await (data.external_num === undefined) ? data.to : data.external_num) + ',' + data.time + ',' + (await (data.sessionid === undefined) ? data.uniqueid : data.sessionid);
    console.log('red key ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.get_key_param = async (from, data) => {
    let key = 'eve_m:' + data.company_id + ':' + data.type + ':' + from + ':' + (await (data.uniqueid === undefined) ? data.sessionid : data.uniqueid);
       console.log('-get_key_param '+key);
    let dat = await getAsync(key);
    if (dat) {
        let send = {
            "from": dat.split(',')[0],
            "to": dat.split(',')[1],
            "time": dat.split(',')[2],
            "sessionid": dat.split(',')[3]
        }
        return send;
    }
    else { return null; }
}
