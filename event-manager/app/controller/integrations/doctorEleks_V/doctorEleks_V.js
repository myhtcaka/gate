//doctorEleks_V v 0.1.1
const redis = require('./redis');
const key = require('./keys');
const dateFormat = require('dateformat');
const request = require('request-promise');

exports.doctorEleks_V = async (all_data) => {
  //   console.log('-doctorEleks_V all_data--- ', all_data);


if ((all_data.event == 'Track') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}
else if ((all_data.event == 'ClickToCall') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}
else if ((all_data.event == 'ClickToCall_D') && (all_data.type == 'out')) {
   await redis.save_click('ClickToCall_D', all_data);
}

 
else if ((all_data.event == 'CRMStart') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
    
}   


else if ((all_data.event == 'Answer') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
    console.log('---- ', await this.doctor_popup(all_data,all_data.from,all_data.to) );
    
    // -doctorEleks_V all_data---  {
    //     event: 'Answer',
    //     privilege: 'call,all',
    //     accountcode: 'user:4705',
    //     channel: 'SIP/80044435704-00125aaa',
    //     channelstate: '6',
    //     channelstatedesc: 'Up',
    //     calleridnum: '80044435704',
    //     calleridname: '704',
    //     connectedlinenum: '80044435704',
    //     connectedlinename: '704',
    //     uniqueid: '1572618191.2231907',
    //     company_id: 434,
    //     group_id: 602,
    //     type: 'in',
    //     from: '80044435704',
    //     to: '80044435704',
    //     time: 1572618197890,
    //     int: {
    //       name: 'doctorEleks-V',
    //       company_id: 434,
    //       content_type: 'application/json',
    //       web_hook: 'http://193.151.89.8:4000/Binotel/api/run',
    //       client_key: null,
    //       partner_key: null
    //     }
    //   }
      
}

else if ((all_data.event == 'Hangup') && (all_data.type == 'in') ) {
    await redis.save_key(all_data);
   
    }

 
} //Do not removethis bracket 


exports.doctor_popup = async (all_data,from, to) => {
    let body = {
      "externalNumber":from,
      "internalNumber":to,
      "dstNumber":from,
      "extNumber":to,
      "generalCallID": all_data.uniqueid,
      "callType":"1",
      "companyID":"X",
      "requestType":"answeredTheCall"
    };
     console.log('SEND body doctor_popup ', body);

    const options = {
      url: all_data.int.web_hook,
      method: 'POST',
      json: true,
      headers: {
        "content-type": all_data.int.content_type
      },
      body
    }
      console.log('options doctor_popup ',options);
  
    try {
      let response = await request(options);
      //  console.log(response);
  
      // console.log('crm resp ', JSON.parse(response));
  
      return response;
    }
    catch (error) {
        console.log('----doctorEleks_V--------  error doctor_popup',error); 
    //  console.log(error);
      return null;
  
    }
  }