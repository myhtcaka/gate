const redis = require("redis");
const rconf = require('../../../servconf').redis;
const { promisify } = require('util');
client = redis.createClient(rconf.port, rconf.host);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);
const expireAsync = promisify(client.expire).bind(client);


// { company_id: 2933,
//     cdr_web_hook: 'https://pbxapi.prostiezvonki.ru/MDhUQTI3/|CLASSprostiezvonki|debug',
//     nummass: [ '80044341201', '80044341202', '80044341203' ],
//     event: 'CRMStart',
//     privilege: 'call,all',
//     accountcode: 'user:9418',
//     channel: 'SIP/80044341202-0009b45e',
//     channelid: '1557920055.2598938',
//     callid: '1557920055.2598935',
//     sessionid: '1557920055.2598935',
//     callerid: '380636414032',
//     calleeid: '80044341202',
//     realcalleeid: '80044341202',
//     direction: 'originate',
//     devicestate: 'RINGING',
//     callback: '0',
//     webphoneaccountcode: '',
//     type: 'in',
//     from: '380636414032',
//     to: '80044341202' }

exports.save_key = async (data) => {
    let key = data.company_id + ':' + data.type + ':' + data.event + ':' + data.channelid;
    let val = data.sessionid+','+data.time;
    console.log('red key ' + key + ' | val ' + val);

    let add = await setAsync(key, val);
    let exp = await expireAsync(key, rconf.key_exp);
    return 1;
}


exports.check_hangup_a = async (data) => {
    let key = data.company_id + ':' + data.type + ':CRMStart:' + (data.uniqueid).split('.')[0] + '.*';
    let keys = await keysAsync(key);
   // console.log('ch_keys ',keys);
    
    let mass = await Promise.all(keys.map(async el => {
        answer = await keysAsync(el.replace('CRMStart', 'Answer'));
       // console.log();
        
        if (answer.length > 0) {
            el = el.split(':')[3];
            return el;
        } else {
            return null;
        }
    }));
   // console.log('mass -- ',mass);
    
    if (mass.indexOf(data.uniqueid) >= 0) {
      //  console.log('send + ');
        return 1;

    }
    else {
     //   console.log('send - ');
        return 0;
    }
}
exports.check_hangup_last = async (data) => {
    let keyC = data.company_id + ':' + data.type + ':CRMStart:' + (data.uniqueid).split('.')[0] + '.*';
    let keyH = data.company_id + ':' + data.type + ':Hangup:' + (data.uniqueid).split('.')[0] + '.*';
    let keysC = await keysAsync(keyC);
    let keysH = await keysAsync(keyH);
    console.log(' long C H '+ keysC.length +' '+keysH.length);
    
    if(keysC.length==keysH.length){
        return 1;
    } else {
        return 0;
    }
}
exports.get_ev_times = async (data) => {
    let keyC = data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid;
    let keyA = data.company_id + ':' + data.type + ':Answer:' + data.uniqueid;
    let timeC = await getAsync(keyC);
    let timeA = await getAsync(keyA);
    if(timeC&&timeA){return time={"call_start_timestamp":timeC.split(',')[1],"call_answer_timestamp":timeA.split(',')[1],"sess":timeC.split(',')[0]}}
    else if (!timeA&&timeC){return time={"call_start_timestamp":timeC.split(',')[1],"call_answer_timestamp":"0","sess":timeC.split(',')[0]}}
    else {console.log(' redis get_ev_times no ev');
    }
    
//    console.log('val C ',timeC);
//    console.log('val A ',timeA);
     
}

    // let mass = await keys.map(async el => {
    //     // [ '2933:in:CRMStart:1557931621.3149007', '2933:in:CRMStart:1557931621.3149006' ]
    //     answer = await keysAsync(el.replace('CRMStart', 'Answer'));
    //     el = {"uni":el.split(':')[3],"is":answer};
    //    // el = el.internal_phone; return el;
    // });
    //   console.log();

    // 
    //  let sess = await getAsync(key);
    // let allk = data.company_id+':'+data.type+':CRMStart:'+data.uniqueid
    //  let getallkeys = await ; 


// setkey = (key,data) => {

//    // client = redis.createClient(rconf.port, rconf.host);
//    return setAsync(key,data);
// }

// getdata_key = (key) =>{
//     return  getAsync(key);
// }


// //check_key('test12');
// (async () =>{
//    // console.log(await setkey('test12','12'));
//    console.log(await getdata_key('test12'));

// })();

