const reqw = require('./services/gate_back/reqwest');
const sql = require('./services/mariadb');


exports.audio_record_params = async (req, res) => {
  //  console.log('- data_all_num controller -');
  let data = await sql.audio_record_param(req.body.sess);
  //  let resp = await reqw.audio_record_params(req.body.sess);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.softswitch_by_key = async (req, res) => {
  // console.log('- softswitch_by_key controller - ',req.body);
  let data = await sql.softswitch_by_key(req.body.apikey);
  //  let resp = await reqw.softswitch_by_key(req.body.apikey);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.lic_extifces_by_key = async (req, res) => {
  // console.log('- lic_extifces_by_key controller - ',req.body);
  let data = await sql.lic_extifces_by_key(req.body.apikey);
  // let resp = await reqw.lic_extifces_by_key(req.body.apikey);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.active_channel_uniqueid = async (req, res) => {
  // console.log('- active_channel_uniqueid controller - ',req.body);
  let data = await sql.active_channel_uniqueid(req.body.uniqueid);
  // let resp = await reqw.active_channel_uniqueid(req.body.uniqueid);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.crm_id_internal = async (req, res) => {
  // console.log('- crm_id_internal controller - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.crm_id_internal(req.body.crm_id);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.all_users_params_by_integretions = async (req, res) => {
  //  console.log('- all_users_params_by_integretions - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.all_users_params_by_integretions(req.body.int_name);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.integretions_params_by_crm_id = async (req, res) => {
  //  console.log('- integretions_params_by_crm_id - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.integretions_params_by_crm_id(req.body.crm_id);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}


exports.integretions_params_by_key = async (req, res) => {
  //  console.log('- integretions_params_by_key - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.integretions_params_by_key(req.body.api_key);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}


// stickylong 
// super_stickiness_minutes
// conditionalRouting (crm_id)
// sticky  (crm_id,num,max_d) 

exports.stickylong = async (req, res) => {
  //  console.log('- stickylong - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.stickylong(req.body.crm_id, req.body.num);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.super_stickiness_minutes = async (req, res) => {
  //  console.log('- super_stickiness_minutes - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.super_stickiness_minutes(req.body.crm_id);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.conditionalRouting = async (req, res) => {
  //  console.log('- conditionalRouting - ',req.body);
  //  let resp = await reqw.conditionalRouting(req.body.crm_id);
  let data = await sql.conditionalRouting(req.body.crm_id);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}

exports.sticky = async (req, res) => {
  //  console.log('- sticky - ',req.body);
  //  let resp = await reqw.crm_id_internal(req.body.crm_id);
  let data = await sql.sticky(req.body.crm_id, req.body.num,req.body.max_d);
  // console.log(resp);
  res.status(200).send({ auth: true, data });
}