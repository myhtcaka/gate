const redis = require('./redis');
const key = require('./keys');
const dateFormat = require('dateformat');
const request = require('request-promise');

exports.streamtele_v3 = async (all_data) => {
  //  console.log('-all_data--- ',all_data);
  let event={};
 
  if(all_data.a_marker==='start_in'){
    // ||(all_data.a_marker==='start_out')){
      
  //      await redis.save_key_v2(all_data);
        //     let inf ={"event":"Start","type":"in","from":all_data.callerid,"to":all_data.calleeid,"via":all_data.calleeid,
        //    "event_time":all_data.time,"call_id":all_data.sessionid,"sessionid":all_data.sessionid};
            event.event="Start";
            event.type="in";
            event.from=all_data.callerid;
            event.to=all_data.calleeid;
            event.via=all_data.calleeid;
            event.sessionid=all_data.sessionid;
            event.event_time=all_data.time;    
            console.log('==== send start in info ',await this.streamtele_send(event,all_data.int));
            //JSON.stringify(event));
        //         console.log('ClickToCall send StartCall ', await this.streamtele_send(body,conf_send));
      //     console.log('send start_in (ring) ',JSON.stringify(inf));
          
      
            
        }else if(all_data.a_marker==='ClickToCall'){
      //    console.log('-ClickToCall all_data--- ',all_data);
          event.event="Start";
          event.type="out";
          event.from=all_data.callerid;
          event.to=all_data.calleeid;
          event.via=all_data.calleeid;
          event.sessionid=all_data.sessionid;
          event.event_time=all_data.time;    
          console.log('==== send start ClickToCall out info ',await this.streamtele_send(event,all_data.int));
          // JSON.stringify(event));

        }else  if(all_data.a_marker==='start_out'){
            let s_info = await redis.get_key_i_v2(all_data);
          //  console.log(' ==== send start_out ',JSON.stringify(s_info));
            if(s_info&&(s_info.transfer||s_info.was_transfer||s_info.transfer_with)){ 
             //   console.log('-=- no send start_out  transfer||was_transfer||transfer_with -=-');
                
            } else if(s_info){
            //    let inf ={"event":"Start","type":"out","from":all_data.callerid,"to":all_data.calleeid.slice(-10),
            //    "start_time":all_data.time,"call_id":all_data.sessionid,"sessionid":all_data.sessionid};
                event.event="Start";
            event.type="out";
            event.from=all_data.callerid;
            event.to=all_data.calleeid;
            event.via=all_data.calleeid;
            event.sessionid=all_data.sessionid;
            event.event_time=all_data.time;    
            console.log('==== send start out info ',await this.streamtele_send(event,all_data.int));
            // JSON.stringify(event));
            //    console.log('==== send start_out ',JSON.stringify(inf));
            
            //    console.log(' --- send start_out ',JSON.stringify(all_data));
            }
            else{
                console.log('-=- no send start_out no info -=-');
                
            }
    //      let trans =  await redis.save_key_v3(all_data);
        //   if(trans){ 
        //     console.log('send start_out transfer ',JSON.stringify(trans));
            
        //   }else {
          //  console.log(' send start_out ',JSON.stringify(all_data));      
            // let inf ={"type":"out","from":all_data.callerid,"to":all_data.calleeid,
            // "start_time":all_data.time,"sessionid":all_data.sessionid};
            // console.log(' ==== send start_out ',JSON.stringify(inf));
            
      //    }
                 
    } else if (all_data.a_marker==='ring'){

     //   await redis.save_key_v3(all_data);
        let s_info = await redis.get_key_i_v2(all_data);

      //  console.log(' -- send ev ring s_info ',s_info);
        if (s_info&&(s_info.transfer)&&(s_info.type==='in')){
       //     console.log(' -- send ev trans info ',JSON.stringify(s_info));
        //    console.log(' -- send ev trans data ',JSON.stringify(all_data));
                 
        }else if(s_info&&(s_info.was_transfer||s_info.transfer_with)&&(s_info.type==='in')){
            event.event="StartCall";
            event.type=s_info.type;
            event.from=s_info.from;
            event.to=s_info.to;
            event.via=s_info.via;
            event.call_id=s_info.callid;
            event.sessionid=all_data.sessionid;
            event.redirecting=1;
            event.additionally=null;
            event.time_start=s_info.time_start;
            event.event_time=s_info.event_time;    
            console.log('==== send StartCall in info ',await this.streamtele_send(event,all_data.int));
            // JSON.stringify(event));
         //   console.log(' === send ev ring was_transfer||transfer_with info ',JSON.stringify(s_info));
       //     console.log(' -- send ev ring data ',JSON.stringify(all_data));
       
        } else if((s_info)&&(s_info.type==='in')){
            event.event="StartCall";
            event.type=s_info.type;
            event.from=s_info.from;
            event.to=s_info.to;
            event.via=s_info.via;
            event.call_id=s_info.callid;
            event.sessionid=all_data.sessionid;
            event.redirecting=null;
            event.additionally=null;
            event.time_start=s_info.time_start;
            event.event_time=s_info.event_time;    
            console.log('==== send StartCall in info ',await this.streamtele_send(event,all_data.int));
            //JSON.stringify(event));
         //   console.log(' === send ev ring info ',JSON.stringify(s_info));
        }
        // ----------------------
        else if(s_info&&(s_info.type==='out')&&(s_info.transfer||s_info.was_transfer||s_info.transfer_with)){
            console.log('-=- no send out ring with  transfer||was_transfer||transfer_with -=-');
             //   console.log(' ==== send ev ring out was_transfer info ',JSON.stringify(s_info));
           // console.log(' ==== send ev ring was_transfer out ',JSON.stringify(all_data));

        }else if(s_info&&(s_info.type==='out')){
            event.event="StartCall";
            event.type=s_info.type;
            event.from=s_info.from;
            event.to=s_info.to;
            event.via=s_info.via;
            event.call_id=s_info.callid;
            event.sessionid=all_data.sessionid;
            event.redirecting=null;
            event.additionally=null;
            event.time_start=s_info.time_start;
            event.event_time=s_info.event_time;    
        console.log('==== send StartCall in info ',await this.streamtele_send(event,all_data.int));
        // JSON.stringify(event));
        //    console.log(' ==== send ev ring out  info ',JSON.stringify(s_info));
          //  console.log(' -- no send ev ring ransfer info out ',JSON.stringify(s_info));
       //     console.log(' -- send ev ring data ',JSON.stringify(all_data));
       
        } else {
            console.log('-=- no send out ring no info -=-');    
        }
       
        
    } else if (all_data.a_marker==='answer'){
       // console.log(' -- answer ',all_data);
    //    await redis.save_key_v3(all_data);
        let s_info = await redis.get_key_i_v2(all_data);
        if(s_info){
            event.event="Answer";
            event.type=s_info.type;
            event.from=s_info.from;
            event.to=s_info.to;
            event.via=s_info.via;
            event.call_id=s_info.callid;
            event.sessionid=s_info.sessionid;
            event.redirecting=(s_info.was_transfer||s_info.transfer_with)?1:null;
            event.additionally=null;
            event.time_start=s_info.time_start;
            event.time_ring=s_info.time_ring;
            event.event_time=s_info.event_time;  
            console.log('==== send Answer in info ',await this.streamtele_send(event,all_data.int));
            // JSON.stringify(event));
            
        }
     //   console.log(' ==== send ev answer info',JSON.stringify(s_info));
      //  if(s_info){
      //      console.log(' ==== send ev answer info',JSON.stringify(s_info));
      //  }

        
    //    console.log(' -- send ev answer data ',JSON.stringify(all_data));

    } else if (all_data.a_marker==='hangup'){
       // console.log(' -- hangup ',all_data);
   //     await redis.save_key_v3(all_data);
        let s_info = await redis.get_key_i_v2(all_data);
        if(s_info){
            let mp3 = 'https://gate.streamtele.com/api/'+all_data.int.name+'/audio?'+await key.get_audio_key2(s_info.sessionid);
            event.event="Hangup";
            event.type=s_info.type;
            event.from=s_info.from;
            event.to=s_info.to;
            event.via=s_info.via;
            event.call_id=s_info.callid;
            event.sessionid=s_info.sessionid;
            event.redirecting=(s_info.was_transfer||s_info.transfer_with)?1:null;
            event.additionally=null;
            event.result=(s_info.answer)?'answer':'no answer';
            event.recordUrl=(s_info.answer)?mp3:((event.redirecting)?mp3:null);
            event.time_start=s_info.time_start;
            event.time_ring=s_info.time_ring;
            event.time_answer=s_info.time_answer;
            event.event_time=s_info.event_time;  

            console.log('==== send Hangup in  ',await this.streamtele_send(event,all_data.int));
            // JSON.stringify(event));
            // await this.streamtele_send(event,all_data.int);
            // let mp3 = 'https://gate.streamtele.com/api/'+all_data.int.name+'/audio?'+await key.get_audio_key2(s_info.sessionid);
        //   console.log(' ==== send ev hangup info',JSON.stringify(s_info));
        }
        
    //    console.log(' -- send ev hangup data ',JSON.stringify(all_data));
    } 

}
    
exports.streamtele_send = async (body,conf_send) => {

  let options = {
      headers: { 
       //   Accept: 'application/json',
          'Content-Type': conf_send.content_type
              },
      uri: conf_send.web_hook,
      method: 'POST',
      body,   
      json: true
      };
      console.log('options send ',options);
      
  try {
      const response = await request(options);
  //    console.log(response);
  return '1';
  }
  catch (error) {
      console.log(error);
  return null;
  }

}