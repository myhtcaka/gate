const jwt = require('jsonwebtoken');
const fs = require('fs');
const rsa_key = fs.readFileSync(require('../../../servconf').jwt.rsa_gate.rsa_key);
const options_one = require('../../../servconf').jwt.options1;




exports.get_one_time_key = async () => {
    return await jwt.sign({}, rsa_key, options_one);

}