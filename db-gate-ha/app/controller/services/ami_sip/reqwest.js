
const request = require('request-promise');
const keys = require('../keys');
const urls = require('../../../../servconf').urls;


exports.data_all_num = async () => {

  const key = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + key },
    uri: 'https://' + urls.db_agent + '/numbers/all_internal',
    json: true,
    body: { "test": "test01" },
    method: 'POST',
    rejectUnauthorized: false,
  };
  //console.log('options ',options);

  try {
    const response = await request(options);
    // console.log('response ',response);
    console.log('send ok');

    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}

  }
  catch (error) {
    console.log('error data_all_num', error);
    console.log('send error');

    return '0';
  }

}

exports.data_all_num_external = async () => {

  const key = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + key },
    uri: 'https://' + urls.db_agent + '/numbers/all_external',
    json: true,
    body: { "test": "test01" },
    method: 'POST',
    rejectUnauthorized: false,
  };
  //console.log('options ',options);

  try {
    const response = await request(options);
    // console.log('response ',response);
    console.log('send ok');

    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}

  }
  catch (error) {
    console.log('error data_all_num_external', error);
    console.log('send error');

    return '0';
  }

}


