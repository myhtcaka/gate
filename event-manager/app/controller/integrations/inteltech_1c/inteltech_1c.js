const redis = require('./redis');
const key = require('./keys');
const dateFormat = require('dateformat');
const request = require('request-promise');



exports.inteltech_1c = async (all_data,conf_send) => {
  // console.log('-1C--- ',all_data);

// ---------------------------------------in ------------------------
if ((all_data.event == 'Track') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}
else if ((all_data.event == 'ClickToCall') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
    let form= 
    { crm_token: all_data.int.client_key,
      cmd: 'event',
      type: 'outgoing',
      callid: all_data.sessionid,
      phone: all_data.from,
      ext: all_data.to };
      console.log('CRMStart out send ', await this.c1_send(form,all_data.int)); 
    
    // red key eve_m:3492:in:ClickToCall:1575269673.38695 | val 380636414032,80044479202,1575269673699,1575269673.38693
   // console.log('CRMStart in all_data ',all_data);
    // {
    //     event: 'ClickToCall',
    //     privilege: 'call,all',
    //     accountcode: 'user:10839',
    //     channel: 'SIP/80044479202-00001600',
    //     channelid: '1575269827.39386',
    //     callid: '1575269827.39381',
    //     sessionid: '1575269827.39380',
    //     callerid: '380636414032',
    //     calleeid: '80044479202',
    //     realcalleeid: '80044479202',
    //     direction: 'originate',
    //     devicestate: 'RINGING',
    //     callback: '1',
    //     webphoneaccountcode: '',
    //     company_id: 3492,
    //     group_id: 1445,
    //     type: 'in',
    //     to: '80044479202',
    //     from: '380636414032',
    //     time: 1575269827973,
    //     int: {
    //       name: '1C',
    //       company_id: 3492,
    //       content_type: 'application/x-www-form-urlencoded',
    //       web_hook: 'http://1cloud.in.ua/podop/hs/telephony/itoolabs',
    //       client_key: '2d9ecf48-2317-4d30-91ff-99c10e636bdf',
    //       partner_key: null
    //     }
    //   }
      

}
//else if ((all_data.event == 'ClickToCall_D') && (all_data.type == 'out')) {
//   await redis.save_click('ClickToCall_D', all_data);
//}
    else if ((all_data.event == 'CRMStart') && (all_data.type=='in') ) {
   //	console.log( 'CRMStart');
        await redis.save_key(all_data);
        let param_t = await redis.get_key_param('Track', all_data);
       //  let external_num = await redis.get_external_num(all_data);
       //     console.log('CRMStart in external_num ',param_t);
       //     console.log('CRMStart in all_data ',all_data);
            let form= 
            { crm_token: all_data.int.client_key,
              cmd: 'event',
              type: 'incoming',
              callid: all_data.sessionid,
              phone: all_data.from,
              ext: all_data.to };
              console.log('CRMStart in send ', await this.c1_send(form,all_data.int)); 
            

            
    // if (external_num){ console.log('external_num', external_num);
    //     let body={ "event":"StartCall", "type": all_data.type,"call_id":all_data.channelid, "event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to,"via":external_num};
      
    //     // console.log('send StartCall ', await this.streamtele_send(body,conf_send));   
    // }
    // else{
    //         console.log('----CRMStart---- no ext num ------------to log  ');
            
    //     }

    }  
    else if ((all_data.event == 'Answer') && (all_data.type=='in')) {
    //    console.log('Answer ',all_data);
   //     all_data.sessionid = 'Answer';
    //    all_data.channelid = all_data.uniqueid;
        await redis.save_key(all_data);

        let param_cs = await redis.get_key_param('CRMStart', all_data);
        let param_cc = await redis.get_key_param('ClickToCall',all_data);
        if(!param_cc){
        //  let external_num = await redis.get_external_num(all_data);
        //     console.log('Answer in external_num ',param_cs);
         //    console.log('Answer in all_data ',all_data);
             let form= 
             { crm_token: all_data.int.client_key,
               cmd: 'event',
          //     type: 'incoming',
               type: 'accepted',
               callid: param_cs.sessionid,
               phone: all_data.from,
               ext: all_data.to };
               console.log('CRMStart in send ', await this.c1_send(form,all_data.int)); 
             }
           else {

            console.log('Answer ClickToCall ');
            

           }  

        // let cc = await redis.check_clicktocall(all_data);
        // if(!cc){
        
        
    //     let external_num = await redis.get_external_num(all_data);
    //     if (external_num){ console.log('external_num', external_num);
    //     let time_start = await redis.get_time_C(all_data);
    // //    console.log('time_start',time_start);
        
    //     let body={"event":"Answer", "type": all_data.type,"call_id":all_data.channelid, "event_time": all_data.time.toString(),"time_start": time_start,"from": all_data.from,"to": all_data.to,"via":external_num};
    //           console.log('send Answer ', await this.streamtele_send(body,conf_send));
    //     }
    //     else{console.log('-------- no ext num ------------to log  ');}
    //     }
    //     else{
    //         // console.log(' ClickToCall  Answer ',all_data);
    //         let body={"event":"Answer", "type": "out","call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": cc.call_start_timestamp,"from": all_data.to,"to": all_data.from,"additionally":"clicktocall"};
    //         console.log('ClickToCall send Answer ', await this.streamtele_send(body,conf_send));
    //     }
        
    }
    else if ((all_data.event == 'Hangup') && (all_data.type=='in')) {
  //      all_data.sessionid = 'Hangup';
   //     all_data.channelid = all_data.uniqueid;
        await redis.save_key(all_data);

        let param_cs = await redis.get_key_param('CRMStart', all_data);
        let param_cc = await redis.get_key_param('ClickToCall',all_data);
        let param_ca = await redis.get_key_param('Answer',all_data);
        
        if(param_cc){
            console.log('Hangup ClickToCall param_cc ',param_cc);
            console.log('Hangup with Answer param_ca ',param_ca);
            if(param_ca){console.log(' ------- Hangup ClickToCall Answer ----- ');
            let form1= 
            { crm_token: all_data.int.client_key,
              cmd: 'event',
              type: 'completed',
              callid: param_cc.sessionid,
              phone: param_cc.from,
              ext: param_cc.to};
              console.log('Hangup cansel out send ', await this.c1_send(form1,all_data.int)); 
              let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?'+await key.get_audio_key2(param_cc.sessionid);
              let form2= 
            { crm_token: all_data.int.client_key,
              cmd: 'history',
              status: 'Success',
              callid: param_cc.sessionid,
              link: mp3,
              duration: (all_data.time-param_cc.time).toString().slice(0,-3) };
              console.log('Hangup history in send ', await this.c1_send(form2,all_data.int)); 
        
        
        
        }
            else{console.log(' ------- Hangup ClickToCall no Answer (in)----- ');
            let form= 
            { crm_token: all_data.int.client_key,
              cmd: 'event',
              type: 'cancelled',
              callid: param_cc.sessionid,
              phone: param_cc.from,
              ext: param_cc.to };
              console.log('Hangup cansel in send ', await this.c1_send(form,all_data.int)); 
        
        
        
        }

        }
        else if(param_ca){
            console.log('Hangup with Answer param_cs ',param_cs);
        console.log('Hangup with Answer param_ca ', param_ca); 
        let form1= 
        { crm_token: all_data.int.client_key,
          cmd: 'event',
          type: 'completed',
          callid: param_cs.sessionid,
          phone: all_data.from,
          ext: all_data.to };
          console.log('Hangup cansel in send ', await this.c1_send(form1,all_data.int)); 
          let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?'+await key.get_audio_key2(param_cs.sessionid);
          let form2= 
        { crm_token: all_data.int.client_key,
          cmd: 'history',
          status: 'Success',
          callid: param_cs.sessionid,
          link: mp3,
          duration: (all_data.time-param_ca.time).toString().slice(0,-3) };
          console.log('Hangup history in send ', await this.c1_send(form2,all_data.int)); 


        }
        else{
            console.log('Hangup cansel ');
            if(param_cc){
                let form= 
            { crm_token: all_data.int.client_key,
              cmd: 'event',
              type: 'cancelled',
              callid: param_cc.sessionid,
              phone: all_data.from,
              ext: all_data.to };
              console.log('Hangup cansel in send ', await this.c1_send(form,all_data.int));
            }
            else{
         //   console.log('Hangup cansel param_cs ',param_cs);
            // Hangup cansel param_cs  {
            //     from: '380636414032',
            //     to: '80044479201',
            //     time: '1575275832493',
            //     sessionid: '1575275831.119095'
            //   }
              
            let form= 
            { crm_token: all_data.int.client_key,
              cmd: 'event',
              type: 'cancelled',
              callid: param_cs.sessionid,
              phone: all_data.from,
              ext: all_data.to };
              console.log('Hangup cansel in send ', await this.c1_send(form,all_data.int)); 
            }
            
        }
       // console.log('Hangup param_cs ',param_cs);
        
        

        // let is = await redis.check_hangup_a(all_data);
        // console.log('--is- answ for hang-', is);
        // if (is == 1) {
        //     let external_num = await redis.get_external_num(all_data);
        //         if (external_num){ console.log('external_num', external_num);
        //         let time_ev = await redis.get_ev_times(all_data);
        //         // console.log('time_ev ',time_ev);
        //         let mp3 = 'https://gate.streamtele.com/api/'+conf_send.name+'/audio?'+await key.get_audio_key2(time_ev.sess);
        //         let body={"event":"Hangup", "type": all_data.type,"call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": time_ev.call_start_timestamp,"time_answer": time_ev.call_answer_timestamp ,"from": all_data.from,"to": all_data.to,"via":external_num,"recordUrl":mp3,"result":"answer"};
        //         console.log('send Hangup ', await this.streamtele_send(body,conf_send));
        //         }
        //         else{console.log('----Hangup is A---- no ext num ------------to log  ');}
        // }
        // else {
        //     all_data.Answer_is=0;
        //     let last = await redis.check_hangup_last(all_data);
        //     console.log('last is ', last);
            
        //     if (last == 1) {
        //         console.log('--Hangup no A send--');
        //         let external_num = await redis.get_external_num(all_data);
        //         if (external_num){ console.log('external_num', external_num);
        //         let time_start = await redis.get_time_C(all_data);
        //         let body={"event":"Hangup", "type": all_data.type,"call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": time_start,"from": all_data.from,"to": all_data.to,"via":external_num,"recordUrl":"","time_answer": "","result":"no answer"};
        //         // console.log('body ',body);
        //         console.log('send Hangup ', await this.streamtele_send(body,conf_send));
        //         }
        //         else{console.log('----Hangup no A---- no ext num ------------to log  ');}
        //     }
        //     else {
        //         let cc = await redis.check_clicktocall(all_data);
        //         if(cc){
        //             //console.log(' ClickToCall Hangup ',cc);
        //             if(cc.call_answer_timestamp!='0'){
        //                 let mp3 = 'https://gate.streamtele.com/api/'+conf_send.name+'/audio?'+await key.get_audio_key2(cc.sess);
        //                 let body={"event":"Hangup", "type": "out","call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": cc.call_start_timestamp,"time_answer": cc.call_answer_timestamp,"from": all_data.to ,"to": all_data.from,"recordUrl":mp3,"result":"answer","additionally":"clicktocall"};    
        //                 console.log('ClickToCall send Hangup A', await this.streamtele_send(body,conf_send));
        //             }
        //             else{
        //             let body={"event":"Hangup", "type": "out","call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": cc.call_start_timestamp,"time_answer": "","from": all_data.to ,"to": all_data.from,"recordUrl":"","result":"no answer","additionally":"clicktocall"};
        //             console.log('ClickToCall send Hangup no A ', await this.streamtele_send(body,conf_send));
        //             }

        //         }
        //         else{console.log('--Hangup no A break --');}
                
        //     }
        // }
      }    
// ---------------------------------------out ------------------------
    else if ((all_data.event == 'CRMStart') && (all_data.type=='out')) {
     //   console.log('---out Start ',all_data);
         await redis.save_key(all_data);
         let form= 
         { crm_token: all_data.int.client_key,
           cmd: 'event',
           type: 'outgoing',
           callid: all_data.sessionid,
           phone: all_data.from,
           ext: all_data.to };
           console.log('CRMStart out send ', await this.c1_send(form,all_data.int)); 

        // let body={ "event":"StartCall", "type": all_data.type,"call_id":all_data.channelid,"event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to};
        // console.log('send StartCall ', await this.streamtele_send(body,conf_send));
    }

    else if ((all_data.event == 'Answer') && (all_data.type=='out')) {
        // all_data.sessionid = 'Answer';
        // all_data.channelid = all_data.uniqueid;
         await redis.save_key(all_data);
        // let time_start = await redis.get_time_C(all_data);
        // all_data.to = await redis.get_out_to(all_data);
        // let body={"event":"Answer", "type": all_data.type,"call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": time_start,"from": all_data.from,"to": all_data.to};
        // console.log('send Answer ', await this.streamtele_send(body,conf_send));
        }

    else if ((all_data.event == 'Hangup') && (all_data.type=='out')) {

        let param_cs = await redis.get_key_param('CRMStart', all_data);
        let param_cc = await redis.get_key_param('ClickToCall',all_data);
        let param_ca = await redis.get_key_param('Answer',all_data);
        
        if(param_cc){
            console.log('Hangup ClickToCall out param_cc ',param_cc);
            console.log('Hangup with Answer out param_ca ',param_ca);

        }
        else if(param_ca){
            console.log('Hangup with Answer out param_cs ',param_cs);
        console.log('Hangup with Answer out param_ca ', param_ca); 
        // param_cs  {
        //     from: '80044479202',
        //     to: '0636414032',
        //     time: '1575468243265',
        //     sessionid: '1575468243.1723416'
          
        let form1= 
        { crm_token: all_data.int.client_key,
          cmd: 'event',
          type: 'completed',
          callid: param_cs.sessionid,
          phone: param_cs.from,
          ext: param_cs.to};
          console.log('Hangup cansel out send ', await this.c1_send(form1,all_data.int)); 
          let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?'+await key.get_audio_key2(param_cs.sessionid);
          let form2= 
        { crm_token: all_data.int.client_key,
          cmd: 'history',
          status: 'Success',
          callid: param_cs.sessionid,
          link: mp3,
          duration: (all_data.time-param_ca.time).toString().slice(0,-3) };
          console.log('Hangup history in send ', await this.c1_send(form2,all_data.int)); 


        }
        else{
            console.log('Hangup cansel out ');
         //   console.log('Hangup cansel param_cs ',param_cs);
            // Hangup cansel param_cs  {
            //     from: '380636414032',
            //     to: '80044479201',
            //     time: '1575275832493',
            //     sessionid: '1575275831.119095'
            //   }
              
            let form= 
            { crm_token: all_data.int.client_key,
              cmd: 'event',
              type: 'cancelled',
              callid: param_cs.sessionid,
              phone: param_cs.from,
              ext: param_cs.to };
              console.log('Hangup cansel in send ', await this.c1_send(form,all_data.int)); 
            
            
        }

        // all_data.channelid = all_data.uniqueid;
        // let is = await redis.check_hangup_a(all_data);
        // all_data.to = await redis.get_out_to(all_data);
        // let time_ev = await redis.get_ev_times(all_data);
        // console.log('out Hangup answer is ',is);
        // if (is == 1) {
        //     let mp3 = 'https://gate.streamtele.com/api/'+conf_send.name+'/audio?'+await key.get_audio_key2(time_ev.sess);
        //     let body={"event":"Hangup", "type": all_data.type,"call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": time_ev.call_start_timestamp,"time_answer": time_ev.call_answer_timestamp ,"from": all_data.from,"to": all_data.to,"recordUrl":mp3,"result":"answer"};
        //     console.log('send Hangup ', await this.streamtele_send(body,conf_send));

        // }
        // else {
        //     let body={"event":"Hangup", "type": all_data.type,"call_id":all_data.channelid,"event_time": all_data.time.toString(),"time_start": time_ev.call_start_timestamp,"from": all_data.from,"to": all_data.to,"recordUrl":"","time_answer": "","result":"no answer"};
        //     console.log('send Answer ', await this.streamtele_send(body,conf_send));  
        // }
        
    }    

 }



exports.c1_send = async (form,conf_send) => {

    let options = {
        headers: { 
         //   Accept: 'application/json',
            'Content-Type': conf_send.content_type
                },
        uri: conf_send.web_hook,
       // qs: { 'crm_token': conf_send.client_key },
        method: 'POST',
        form,   
      
        };
        console.log('options send ',options);

        
    try {
        const response = await request(options);
        console.log(response);
    return '1';
    }
    catch (error) {
        console.log(error.body);
    return null;
    }

}




   