const routes = require('express').Router();
const check= require('../middleware/check');
const sip = require('../controller/sip/sip');
const sipu = require('../controller/sipu/sipu');
const kyivstar_iskratel = require('../controller/kyivstar_iskratel/kyivstar_iskratel');
const rabbit_crm = require('../controller/rabbit_crm/rabbit_crm');


//    ---sip---
routes.post('/sip/call_GenerateCall',[check.authentication],sip.call_GenerateCall);
routes.post('/sip/call_Originate',[check.authentication],sip.call_Originate);
routes.post('/sip/call_api_stream',[check.authentication],sip.call_api_stream);
routes.post('/sip/call_sip_stream',[check.authentication],sip.call_sip_stream);
//        ---sipu---
routes.post('/sipu/call_GenerateCall',[check.authentication],sipu.call_GenerateCall);
routes.post('/sipu/call_Originate',[check.authentication],sipu.call_Originate);
routes.post('/sipu/call_api_stream',[check.authentication],sipu.call_api_stream);
routes.post('/sipu/call_sip_stream',[check.authentication],sipu.call_sip_stream);
// ---- kyivstar_iskratel ---
routes.post('/kyivstar_iskratel/call_stream_crm',[check.authentication],kyivstar_iskratel.call_stream_crm);
//       ---rabbitmq---
routes.post('/rabbit_crm/popup_start_call',[check.authentication],rabbit_crm.popup_start_call);

// call_actions_V02
module.exports = routes;