const { Rabbit } = require('rabbit-queue');
const rabbit_crm = require('../../../servconf').rabbit.rabbit_crm;

exports.popup_start_call = async (req, res) => {
   // req.body.to внутрения линия
   // req.body.from внешний
   // ----------- data or null
   
   // req.body.eventname
   // req.body.direction
   // req.body.subevent
   // req.body.sessionId
   // req.body.time           

    let rabbit = new Rabbit(rabbit_crm);

  await rabbit.publishExchange('ex'+req.body.to , req.body.to,
  {
      "CallerId": req.body.from ,
      "EventName":(req.body.eventname)?req.body.eventname:"CRMStart",
      "Direction":(req.body.direction)?req.body.direction:"In",
      "SubEvent":(req.body.subevent)?req.body.subevent:"NewstateEvent|Dial",
      "SessionId":(req.body.sessionId)?req.body.sessionId:Date.now(),
      "Time":(req.body.time)?req.body.time:Date.now()
  }
  ,{reconnect: true})
  .then( () => { 
     return rabbit.close(); 
  })
  .catch(error  => {console.log('error  '); 
 // return rabbit.close();
});


    
}
