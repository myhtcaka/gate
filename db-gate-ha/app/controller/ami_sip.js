const reqw = require('./services/ami_sip/reqwest');
const sql = require('./services/mariadb');

// data_all_internal_num
exports.data_all_internal_num = async (req, res) => {
  console.log('- data_all_num controller -');
  let data = await sql.all_internal();
  // console.log(resp);

  res.status(200).send({ auth: true, data });
}

// data-all-internal-num
exports.data_all_external_num = async (req, res) => {
  //  console.log('- data_all_num controller -');
  let data = await sql.all_external();
  // console.log(resp);

  res.status(200).send({ auth: true, data });
}

