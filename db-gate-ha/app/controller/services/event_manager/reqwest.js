
const request = require('request-promise');
const keys = require('../keys');
const urls = require('../../../../servconf').urls;


exports.integrations_data = async () => {

  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/integrations',
    json: true,
    body: {},
    method: 'POST',
    //  rejectUnauthorized: false,
  };
  //  console.log('options ',options);
  try {
    const response = await request(options);
    //    console.log('response ',response);
    console.log('send ok');
    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}
  }
  catch (error) {
    console.log('error integrations_data ', error);
    console.log('send error');
    return '0';
  }
}

