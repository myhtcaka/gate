const Ami = require('asterisk-manager');

const conf_ami = require('../../../../servconf').ami;
// (conf_ami.port, conf_ami.host, conf_ami.login, conf_ami.pass, true);
// ('5038', 'sip.streamtele.com', 'amilogin', '1{[6WAT@', true);

// dropcall/pickupchan/chanspy/conference/prompter/transfer
//   data: {
//     action: 'chanspy',
//     dst: '80044340203',
//     uniqueid: '1565858340.1767542',
//     crm_name: 'Moituristy_integration',
//     crm_id: 2942,
//     softswitch_user_group_id: 1247,
//     softswitch_login: 'turisty',
//     softswitch_password: 'oisjaflk4535345'
//     }
//   data: {
//     action: 'dropcall',
//     dst: '80044340203',
//     uniqueid: '1565858340.1767542',
//     crm_name: 'Moituristy_integration',
//     crm_id: 2942,
//     softswitch_user_group_id: 1247,
//     softswitch_login: 'turisty',
//     softswitch_password: 'oisjaflk4535345'
//     }
//     console.log('- data_all_num controller -');
// let resp = await reqw.data_all_num();
//     console.log("call_actions",req.body);

//     res.status(200).send({ auth: true, data: "ok"});


exports.dropcall = async (data) => {
    console.log("data dropcall ", data);
    let ami = new Ami(conf_ami.port, conf_ami.host, conf_ami.login, conf_ami.pass, true);
    return await new Promise((resolve, reject) => {
        ami.action({
            'action': 'Hangup',
            'channel': data.channel
        }, async (err, res) => {
            await resolve(res);
            await reject(err);
            ami.disconnect();
        });
    });
}

exports.chanspy = async (data) => {
    console.log("data chanspy ", data);
//    let ami = new Ami('5038', 'sip.streamtele.com', 'web_80044340201', 'RsZzn,J-', true);
    let ami = new Ami('5038', 'sip.streamtele.com', data.softswitch_login, data.softswitch_password, true);
    // 'SIP/'+(data.channel.split('@')[0].split('/')[1])
    let act = {
        'action': 'Originate',
        'channel': 'Local/' + data.dst + '@crm_callback/n',
        'priority': 1,
        //'waittime': 3200,
          'callerid': data.dst,
        'application': 'ChanSpy',
        'data': data.channel+ ',q'
        // 'SIP/' + (data.channel.split('@')[0].split('/')[1]) + ',q',

    };
    // console.log('act ',act);

    return await new Promise((resolve, reject) => {
        ami.action(act, async (err, res) => {
            await resolve(res);
            await reject(err);
            ami.disconnect();
        });
    });
}

// pickupchan/conference/prompter/transfer
exports.pickupchan = async (data) => {
    console.log("data chanspy ", data);
//    let ami = new Ami('5038', 'sip.streamtele.com', 'web_80044340201', 'RsZzn,J-', true);
    let ami = new Ami('5038', 'sip.streamtele.com', data.softswitch_login, data.softswitch_password, true);
    // 'SIP/'+(data.channel.split('@')[0].split('/')[1])
    let act = {
        'action': 'Originate',
        'channel': 'Local/' + data.dst + '@crm_callback/n',
        'priority': 1,
        'callerid': data.dst,
        'application': 'PickupChan',
        'data': data.channel 
        // 'SIP/' + (data.channel.split('@')[0].split('/')[1]) ,

    };
    // console.log('act ',act);

    return await new Promise((resolve, reject) => {
        ami.action(act, async (err, res) => {
            await resolve(res);
            await reject(err);
            ami.disconnect();
        });
    });
}

// conference/prompter/transfer
exports.conference = async (data) => {
    console.log("data chanspy ", data);
//    let ami = new Ami('5038', 'sip.streamtele.com', 'web_80044340201', 'RsZzn,J-', true);
let ami = new Ami('5038', 'sip.streamtele.com', data.softswitch_login, data.softswitch_password, true);  
    // 'SIP/'+(data.channel.split('@')[0].split('/')[1])
    let act = {
        'action': 'Originate',
        'channel': 'Local/' + data.dst + '@crm_callback/n',
        'priority': 1,
        'callerid': data.dst,
        'application': 'ChanSpy',
        'data': data.channel+ ',qB'
        // 'SIP/' + (data.channel.split('@')[0].split('/')[1]) + ',qB',

    };
    // console.log('act ',act);

    return await new Promise((resolve, reject) => {
        ami.action(act, async (err, res) => {
            await resolve(res);
            await reject(err);
            ami.disconnect();
        });
    });
}

// /prompter/transfer
exports.prompter = async (data) => {
    console.log("data chanspy ", data);
//    let ami = new Ami('5038', 'sip.streamtele.com', 'web_80044340201', 'RsZzn,J-', true);
let ami = new Ami('5038', 'sip.streamtele.com', data.softswitch_login, data.softswitch_password, true);
// 'SIP/'+(data.channel.split('@')[0].split('/')[1])
    let act = {
        'action': 'Originate',
        'channel': 'Local/' + data.dst + '@crm_callback/n',
        'priority': 1,
        'callerid': data.dst,
        'application': 'ChanSpy',
        'data': data.channel+ ',qw'
        // 'SIP/' + (data.channel.split('@')[0].split('/')[1]) + ',qB',

    };
    // console.log('act ',act);

    return await new Promise((resolve, reject) => {
        ami.action(act, async (err, res) => {
            await resolve(res);
            await reject(err);
            ami.disconnect();
        });
    });
}
// Action: Atxfer
// Channel: SIP/200-00000001 //источник (кого переводим)
// Exten: SIP/902 // приемник (куда переводим)
// Context: from-internal-xfer
// Priority: 1
// /prompter/transfer
exports.transfer = async (data) => {
    console.log("data chanspy ", data);
//    let ami = new Ami('5038', 'sip.streamtele.com', 'web_80044340201', 'RsZzn,J-', true);
let ami = new Ami('5038', 'sip.streamtele.com', data.softswitch_login, data.softswitch_password, true);
// 'SIP/'+(data.channel.split('@')[0].split('/')[1])
    let act = {
        'action': 'Originate',
        'channel': data.channel,
        'priority': 1,
        'exten': data.dst,
        'application': 'Atxfer',
        'context': 'from-internal-xfer'
                // 'SIP/' + (data.channel.split('@')[0].split('/')[1]) + ',qB',

    };
    // console.log('act ',act);

    return await new Promise((resolve, reject) => {
        ami.action(act, async (err, res) => {
            await resolve(res);
            await reject(err);
            ami.disconnect();
        });
    });
}


// 'action':'originate',
//       'channel':'Local/80044340201@crm_callback/n',
//       'exten': '380636414032',
//       'priority':1,
//       'callerid':'80044340201',

exports.clicktocall = async (data) => {
    console.log("data chanspy ", data);
//    let ami = new Ami('5038', 'sip.streamtele.com', 'web_80044340201', 'RsZzn,J-', true);
let ami = new Ami('5038', 'sip.streamtele.com', data.softswitch_login, data.softswitch_password, true);
// 'SIP/'+(data.channel.split('@')[0].split('/')[1])
    let act = {
        'action': 'Originate',
        'channel': 'Local/' + data.dst + '@crm_callback/n',
        'priority': 1,
        'exten': data.channel,
        'callerid': 'Stream CRM <'+ data.dst+'>'
                        // 'SIP/' + (data.channel.split('@')[0].split('/')[1]) + ',qB',

    };
     console.log('act ',act);

//    return await new Promise((resolve, reject) => {
        ami.action(act, async (err, res) => {
//        console.log('res ',res);
//            await resolve(res);
//            await reject(err);
            ami.disconnect();
//        });
    });
    return 'Originate successfully queued';
}

exports.clicktocall_V02 = async (data) => {
    console.log('data ',data);
    
    let ami = new Ami(conf_ami.port, conf_ami.host, conf_ami.login, conf_ami.pass, true);
    let act = {
        'action': 'GenerateCall',
        'account':data.data_m,
        'callerid': data.name+'<'+ data.dst+'>',
        'channel': 'Local/' + data.dst + '@crm_callback/n',
        'context': 'crm_callback',
        'priority': 1,
        'exten':'s',
   //   'timeout': 32000,
        'variable': {'CALLBACK_ID':data.dst,'DIAL_ID': data.channel,'USERNAME':data.dst}
    };
    console.log('act ',act);
    //    return await new Promise((resolve, reject) => {
            ami.action(act, async (err, res) => {
            console.log('res ',res);
    //            await resolve(res);
    //            await reject(err);
                ami.disconnect();
    //        });
        });    
    return 'Originate successfully queued';

}
