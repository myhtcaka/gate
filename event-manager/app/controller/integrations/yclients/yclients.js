// yclients v 0.1.1
const redis = require('./redis');
const key = require('./keys');
const dateFormat = require('dateformat');
const request = require('request-promise');

exports.yclients = async (all_data) => {
    console.log('-yclients all_data--- ',all_data);


if ((all_data.event == 'Track') && (all_data.type == 'in')) {
    //  console.log( 'moysklad Track in');
    await redis.save_key(all_data);
}
else if ((all_data.event == 'ClickToCall') && (all_data.type == 'in')) {
    console.log('ClickToCall ', all_data);

    //    all_data.type='out'
    await redis.save_key(all_data);
    //   let body={ "event":"StartCall", "type": "out","call_id":all_data.channelid,"event_time": all_data.time.toString(),"from": all_data.from,"to": all_data.to,"additionally":"clicktocall"};
    //   console.log('ClickToCall send StartCall ', await this.streamtele_send(body,conf_send));

}

else if ((all_data.event == 'CRMStart') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
    let param_t = await redis.get_key_param('Track', all_data);
    if (param_t) {
        //console.log('PARAM_T ', param_t);
        let body = {
            "command": "event",
            "type": "incoming",
            "crm_token": all_data.int.client_key,
            "phone": all_data.from,
            "user": (all_data.to).slice(-3),
            "diversion": param_t.to
        };
        let options = {
            "url": all_data.int.web_hook,
            "headers":
            {
                "Authorization": "Bearer " + all_data.int.partner_key,
                "Content-Type": all_data.int.content_type
            },
            body,
            "method": "POST",
            "json": true

        };
        console.log('StartCall in yclients_send ', await this.yclients_send(options));
    }
    else {
        console.log('--no track (out call)--');

    }
}

else if ((all_data.event == 'Answer') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
}

else if ((all_data.event == 'Hangup') && (all_data.type == 'in')) {
    await redis.save_key(all_data);
    let param_a = await redis.get_key_param('Answer', all_data);
    console.log('param_a ', param_a);
    if (param_a) {
        let param_сс = await redis.get_key_param('ClickToCall', all_data);
        console.log('param_сс ',param_сс);

        if (!param_сс) {
            let param_c = await redis.get_key_param('CRMStart', all_data);
            //console.log("PARAM_T", param_c);
            console.log('Hangup C param_ ', param_c);
            //     console.log('Hangup A param_ ', param_a);
            // console.log(new Date(param_a.time));
            //  console.log(new Date(all_data.time));
            let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(param_c.sessionid);
            //   console.log('Hangup A mp3 ',mp3);
            let body = {
                "command": "history",
                "type": "incoming",
                "crm_token": all_data.int.client_key,
                "phone": all_data.from,
                "user": (all_data.to).slice(-3),
                "diversion": param_c.to,
                "duration": (all_data.time).toString().slice(0,-3) - (param_a.time).toString().slice(0,-3),
                "link": mp3,
                "call_id": (all_data.uniqueid).slice(-7),
                "status": "success",
                "date":  (new Date(parseInt(param_a.time))).toISOString().slice(0, 19) + "+00:00"
            }
            let options = {
                "url": all_data.int.web_hook,
                "headers":
                {
                    "Authorization": "Bearer " + all_data.int.partner_key,
                    "Content-Type": all_data.int.content_type
                },
                body,
                "method": "POST",
                "json": true

            };
            console.log('Hangup Answer yclients_send ', await this.yclients_send(options));
        }
    } else {
        console.log('Hangup no A all_data', all_data);
        //let param_t = await redis.get_key_param('Track', all_data);
        let body = {
            "command": "history",
            "type": "incoming",
            "crm_token": all_data.int.client_key,
            "phone": all_data.from,
            "user": (all_data.to).slice(-3),
            "diversion": all_data.to,
            "duration": 0, 
            "link": "#",
            "call_id": (all_data.uniqueid).slice(-7),
            "status": "missed",
            "date": (new Date(parseInt(all_data.time))).toISOString().slice(0, 19) + "+00:00"
        };
        let options = {
            "url": all_data.int.web_hook,
            "headers":
            {
                "Authorization": "Bearer " + all_data.int.partner_key,
                "Content-Type": all_data.int.content_type
            },
            body,
            "method": "POST",
            "json": true
        };
        console.log('Answer yclients_send ', await this.yclients_send(options));
    }
}

//------------------------out----------------------

else if ((all_data.event == 'CRMStart') && (all_data.type == 'out')) {
    //console.log( 'CRMStart out yclients ',all_data);
    await redis.save_key(all_data);
    //console.log('StartCall out yclients_save ');
}

else if ((all_data.event == 'Answer') && (all_data.type == 'out')) {
    //console.log('################DATA TO SAVE: ', all_data);
    await redis.save_key(all_data);
}


 else if ((all_data.event == 'Hangup') && (all_data.type == 'out')) {
     await redis.save_key(all_data);
     //console.log('All Data before redis: ', all_data);
     let param_a = await redis.get_key_param('Answer', all_data);
     if (param_a) {
            let param_c = await redis.get_key_param('CRMStart', all_data);
            //console.log('-- send out Hangup A ', param_a);
            //console.log('-- send out Hangup C ', param_c);
            //console.log('-- send out Hangup C ', param_c);
            console.log('out Hangup A ', all_data);
         let mp3 = 'https://gate.streamtele.com/api/streamteleV2/audio?' + await key.get_audio_key2(all_data.uniqueid);
         let body = {
             "command": "history",
             "type": "outgoing",
             "crm_token": all_data.int.client_key,
             "phone": param_c.to,
             "user": (all_data.from).slice(-3),
             "diversion": all_data.connectedlinenum,
             "duration": (all_data.time).toString().slice(0,-3) - (param_a.time).toString().slice(0,-3),
             "link": mp3,
             "call_id": (all_data.uniqueid).slice(-7),
             "status": "success",
             "date":  (new Date(parseInt(param_a.time))).toISOString().slice(0, 19) + "+00:00"
         }
         let options = {
             "url": all_data.int.web_hook,
             "headers":
             {
                 "Authorization": "Bearer " + all_data.int.partner_key,
                 "Content-Type": all_data.int.content_type
             },
             body,
             "method": "POST",
             "json": true
         };
         console.log('Hangup out yclients_send ', await this.yclients_send(options));
     }
     else {
         console.log('-- out Hangup no A ');
         console.log('Hangup no A all_data', all_data);
         let param_c = await redis.get_key_param('CRMStart', all_data);
         let body = {
            "command": "history",
            "type": "outgoing",
            "crm_token": all_data.int.client_key,
            "phone": param_c.to,
            "user": (all_data.from).slice(-3),
            "diversion": all_data.connectedlinenum,
            "duration": 0,
            "link": "#",
            "call_id": (all_data.uniqueid).slice(-7),
            "status": "missed",
            "date":  (new Date(parseInt(param_c.time))).toISOString().slice(0, 19) + "+00:00"
         };
         let options = {
             "url": all_data.int.web_hook,
             "headers":
             {
                 "Authorization": "Bearer " + all_data.int.partner_key,
                 "Content-Type": all_data.int.content_type
             },
             body,
             "method": "POST",
             "json": true
         };
         console.log('Answer yclients_send ', await this.yclients_send(options));
     }
 } 

}


exports.yclients_send = async (options) => {
     console.log(' --- yclients_send options-- ',options.body);
     console.log(' --- yclients_send options .events-- ', options);
    try {
        const response = await request(options);
        //    console.log(response);
        return '1';
    }
    catch (error) {
        console.log(error);
        return null;
    }
}
