const routes = require('express').Router();
const check = require('../middleware/check');
const gate_back = require('../controller/gate_back');

routes.post('/audio_record_params', [check.authentication], gate_back.audio_record_params);
routes.post('/softswitch_by_key', [check.authentication], gate_back.softswitch_by_key);
routes.post('/lic_extifces_by_key', [check.authentication], gate_back.lic_extifces_by_key);
// lic_extifces_by_key
routes.post('/active_channel_uniqueid', [check.authentication], gate_back.active_channel_uniqueid);
routes.post('/crm_id_internal', [check.authentication], gate_back.crm_id_internal);
routes.post('/params_by_integretions', [check.authentication], gate_back.all_users_params_by_integretions);
routes.post('/integretions_params_by_crm_id', [check.authentication], gate_back.integretions_params_by_crm_id);
routes.post('/integretions_params_by_key', [check.authentication], gate_back.integretions_params_by_key);
routes.post('/stickylong', [check.authentication], gate_back.stickylong);
routes.post('/super_stickiness_minutes', [check.authentication], gate_back.super_stickiness_minutes);
routes.post('/conditionalRouting', [check.authentication], gate_back.conditionalRouting);
routes.post('/sticky', [check.authentication], gate_back.sticky);


// integretions_params_by_key(req.body.api_key);
// stickylong 
// super_stickiness_minutes
// conditionalRouting (crm_id)
// sticky  (crm_id,num,max_d) 


module.exports = routes;