const redis = require("redis");
const rconf = require('../../../servconf').redis;

// var redis = require("redis"),
//     client = redis.createClient(rconf.port, rconf.host);
 
// // if you'd like to select database 3, instead of 0 (default), call
// // client.select(3, function() { /* ... */ });
 
// client.on("error", function (err) {
//     console.log("Error " + err);
// });

const { promisify } = require('util');
client = redis.createClient(rconf.port, rconf.host);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);
const expireAsync = promisify(client.expire).bind(client);

// { event: 'CRMStart',
//   privilege: 'call,all',
//   accountcode: 'user:9419',
//   channel: 'SIP/80044341203-0010f3a9',
//   channelid: '1558701051.5878848',
//   callid: '1558701051.5878843',
//   sessionid: '1558701051.5878843',
//   callerid: '380636414032',
//   calleeid: '80044341203',
//   realcalleeid: '80044341203',
//   direction: 'originate',
//   devicestate: 'RINGING',
//   callback: '0',
//   webphoneaccountcode: '',
//   compsny_id: 2933,
//   group_id: 1241,
//   type: 'in',
//   from: '380636414032',
//   to: '80044341203',
//   time: 1558701051636,
//   int: 
//    [ { name: 'prostiezvonki',

// { event: 'Track',
//   privilege: 'call,all',
//   accountcode: 'gateway:2698',
//   channel: 'SIP/0443002000-000250df',
//   channelid: '1559215054.521145',
//   callid: '1559215054.521145',
//   sessionid: '1559215054.521145',
//   callerid: '380636414032',
//   calleeid: '380443002160',
//   realcalleeid: '380443002160',
//   direction: 'answer',
//   devicestate: 'UNKNOWN',
//   callback: '0',
//   webphoneaccountcode: '',
//   company_id: 3041,
//   group_id: 1280,
//   type: 'in',
//   external_num: '380443002160',
//   from: '380636414032',
//   time: 1559215054463,





exports.save_key = async (data) => {
 //  console.log(data);
    let key = data.company_id + ':' + data.type + ':' + data.event + ':' + data.channelid;
    if((data.event==='CRMStart')&&(data.type==='out')){
        let val = data.sessionid+','+data.time+','+data.to;
        console.log('red key ' + key + ' | val ' + val);
        let add = await setAsync(key, val);
    let exp = await expireAsync(key, rconf.key_exp);
    return 1;
    }
    else if(data.event==='Track'){
    let val = data.external_num
    console.log('red key ' + key + ' | val ' + val);
    let add = await setAsync(key, val);
    let exp = await expireAsync(key, rconf.key_exp);
    return 1;
    }
    else{let val = data.sessionid+','+data.time
    console.log('data.event '+data.event+' red key ' + key + ' | val ' + val);
    let add = await setAsync(key, val);
    let exp = await expireAsync(key, rconf.key_exp);
    return 1;
}
    
   // console.log('red key ' + key + ' | val ' + val);

    
}


exports.check_hangup_a = async (data) => {
 //   if((all_data.event == 'Hangup') && (all_data.type=='out')) {
 //       let key = data.company_id + ':' + data.type + ':CRMStart:' + (data.uniqueid).split('.')[0] + '.*';
 //   let keys = await keysAsync(key);
 //   console.log('Hangup out key | ch_keys ',key,keys);
 //   return 0;

 //   }
//    else{
    let key = data.company_id + ':' + data.type + ':CRMStart:' + (data.uniqueid).split('.')[0] + '.*';
    let keys = await keysAsync(key);
    console.log('key | ch_keys ',key,keys);
    
    let mass = await Promise.all(keys.map(async el => {
        answer = await keysAsync(el.replace('CRMStart', 'Answer'));
       // console.log();
        
        if (answer.length > 0) {
            el = el.split(':')[3];
            return el;
        } else {
            return null;
        }
    }));
   // console.log('mass -- ',mass);
    
    if (mass.indexOf(data.uniqueid) >= 0) {
      //  console.log('send + ');
        return 1;

    }
    else {
     //   console.log('send - ');
        return 0;
    }
//  }
}
exports.check_hangup_last = async (data) => {
    let keyC = data.company_id + ':' + data.type + ':CRMStart:' + (data.uniqueid).split('.')[0] + '.*';
    let keyH = data.company_id + ':' + data.type + ':Hangup:' + (data.uniqueid).split('.')[0] + '.*';
    let keysC = await keysAsync(keyC);
    let keysH = await keysAsync(keyH);
    console.log(' long C H '+ keysC.length +' '+keysH.length);
    
    if(keysC.length==keysH.length){
        return 1;
    } else {
        return 0;
    }
}

exports.check_clicktocall = async (data) => {
    let keyC = data.company_id + ':' + data.type + ':ClickToCall:' + data.uniqueid;
   console.log('keyC call ', keyC);
   
    let timeC = await getAsync(keyC);
   // let keysH = await keysAsync(keyH);
    console.log(' is  ClicToCall '+ timeC);
    
    if(timeC){
        let keyA = data.company_id + ':' + data.type + ':Answer:' + data.uniqueid;
      //  console.log('keyA call',keyA);
        let timeA = await getAsync(keyA);
        if(timeC&&timeA){return time={"call_start_timestamp":timeC.split(',')[1],"call_answer_timestamp":timeA.split(',')[1],"sess":timeC.split(',')[0]}}
    else if (!timeA&&timeC){return time={"call_start_timestamp":timeC.split(',')[1],"call_answer_timestamp":"0","sess":timeC.split(',')[0]}}
    else {console.log(' redis get_ev_times no ev');
    //    console.log(' is C ClicToCall '+ keysC);
    //    console.log(' is A  ClicToCall '+ timeA);
        return null;
        } 
    }
    else {return null;}
}

exports.get_ev_times = async (data) => {

    let keyC = data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid;
    let keyA = data.company_id + ':' + data.type + ':Answer:' + data.uniqueid;
    let timeC = await getAsync(keyC);
    let timeA = await getAsync(keyA);
    if(timeC&&timeA){return time={"call_start_timestamp":timeC.split(',')[1],"call_answer_timestamp":timeA.split(',')[1],"sess":timeC.split(',')[0]}}
    else if (!timeA&&timeC){return time={"call_start_timestamp":timeC.split(',')[1],"call_answer_timestamp":"0","sess":timeC.split(',')[0]}}
    else {console.log(' redis get_ev_times no ev');
    }
}

    exports.get_out_to = async (data) => {
        let keyC = data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid;
     //   console.log('keyC',keyC);
        let timeC = await getAsync(keyC);
        console.log('get_out_to key,data ',keyC,timeC);
        if(timeC){return timeC.split(',')[2];}
        else {console.log(' redis get_ev_times no ev');
        return null;
        }

    }

    exports.get_time_C = async (data) => {
        let keyC = data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid;
     //   console.log('keyC',keyC);
        let timeC = await getAsync(keyC);
        console.log('timeC ',timeC);
        if(timeC){return timeC.split(',')[1];}
        else {console.log(' redis get_ev_times no ev');
        return null;
        }

    }

    exports.get_external_num = async (data) => {
     //   console.log('calleridnum get ext -- ',data);
    if((data.event==='CRMStart')&&(data.type==='in')){
        let keyX = data.company_id + ':' + data.type + ':Track:' + data.sessionid;
        let external_num = await getAsync(keyX);
        if(external_num){return external_num;} 
            //timeC.split(',')[2];}
        else {console.log(' redis get_external_num no external_num ------------to log ');
        return null;
        }
    }
    else if((data.event==='Answer')&&(data.type==='in')){
       // let keyC = data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid;
        let ssC = await getAsync(data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid);
        // let keyX = data.company_id + ':' + data.type + ':Track:' + ssC.split(',')[0];
        //let external_num = await getAsync(keyX);
        console.log(ssC);
        
        if (ssC){
            let external_num = await getAsync(data.company_id + ':' + data.type + ':Track:' + ssC.split(',')[0]);
               if(external_num){return external_num;} 
            //timeC.split(',')[2];}
            console.log(' redis get_external_num no external_num ------------to log ');
            return null;
                  
        }
        else{return null;}
        //(await getAsync(data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid)).split(',')[0])

        
        

    }
    else if((data.event==='Hangup')&&(data.type==='in')){
        let keyC = data.company_id + ':' + data.type + ':CRMStart:' + data.uniqueid;
        let ssC = await getAsync(keyC);
        let keyX = data.company_id + ':' + data.type + ':Track:' + ssC.split(',')[0];
        let external_num = await getAsync(keyX);
        if(external_num){return external_num;} 
            //timeC.split(',')[2];}
        else {console.log(' redis get_external_num no external_num ------------to log ');
        return null;
        }
      }
      
    

}
   
    
//    console.log('val C ',timeC);
//    console.log('val A ',timeA);
     


    // let mass = await keys.map(async el => {
    //     // [ '2933:in:CRMStart:1557931621.3149007', '2933:in:CRMStart:1557931621.3149006' ]
    //     answer = await keysAsync(el.replace('CRMStart', 'Answer'));
    //     el = {"uni":el.split(':')[3],"is":answer};
    //    // el = el.internal_phone; return el;
    // });
    //   console.log();

    // 
    //  let sess = await getAsync(key);
    // let allk = data.company_id+':'+data.type+':CRMStart:'+data.uniqueid
    //  let getallkeys = await ; 


// setkey = (key,data) => {

//    // client = redis.createClient(rconf.port, rconf.host);
//    return setAsync(key,data);
// }

// getdata_key = (key) =>{
//     return  getAsync(key);
// }


// //check_key('test12');
// (async () =>{
//    // console.log(await setkey('test12','12'));
//    console.log(await getdata_key('test12'));

// })();

