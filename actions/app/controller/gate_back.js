
const action = require('./services/gate_back/ami_actions');

exports.call_actions = async (req, res) => {
      if(req.body.data.action==='dropcall'){ res.status(200).send({ auth: true, message: await action.dropcall(req.body.data)});}
      else if (req.body.data.action==='pickupchan') {res.status(200).send({ auth: true, message: await action.pickupchan(req.body.data)});}
      else if (req.body.data.action==='chanspy') {res.status(200).send({ auth: true, message: await action.chanspy(req.body.data)});}
      else if (req.body.data.action==='conference') {res.status(200).send({ auth: true, message: await action.conference(req.body.data)});}
      else if (req.body.data.action==='prompter') {res.status(200).send({ auth: true, message: await action.prompter(req.body.data)});}
      else if (req.body.data.action==='transfer') {res.status(200).send({ auth: true, message: await action.transfer(req.body.data)});}
      else if (req.body.data.action==='clicktocall') {res.status(200).send({ auth: true, message: await action.clicktocall(req.body.data)});}
      else  { res.status(200).send({ auth: true, message: "bad request"});}
}

exports.call_actions_V02 = async (req, res) => {
      if (req.body.data.action==='clicktocall') {res.status(200).send({ auth: true, message: await action.clicktocall_V02(req.body.data)});} // GenerateCall
      else  { res.status(200).send({ auth: true, message: "bad request"});}

}