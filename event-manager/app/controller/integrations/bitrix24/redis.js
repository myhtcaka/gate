const redis = require("redis");
const rconf = require('../../../../servconf').redis;

const { promisify } = require('util');
client = redis.createClient(rconf.port, rconf.host);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);
const expireAsync = promisify(client.expire).bind(client);

// event: 'Answer',
//   privilege: 'call,all',
//   accountcode: 'user:9437',
//   channel: 'SIP/80044340202-00027338',
//   channelstate: '6',
//   channelstatedesc: 'Up',
//   calleridnum: '380636414032',
//   calleridname: '',
//   connectedlinenum: '380636414032',
//   connectedlinename: '',
//   uniqueid: '1567427032.371516',
//   company_id: 2942,
//   group_id: 1247,
//   type: 'in',
//   from: '380636414032',
//   to: '80044340202',
//   time: 1567427036629,

// event: 'CRMStart',
// privilege: 'call,all',
// accountcode: 'user:9437',
// channel: 'SIP/80044340202-0001bcaa',
// channelid: '1567419756.277565',
// callid: '1567419756.277562',
// sessionid: '1567419756.277562',
// callerid: '+380636414032',
// calleeid: '80044340202',
// realcalleeid: '80044340202',
// direction: 'originate',
// devicestate: 'RINGING',
// callback: '0',
// webphoneaccountcode: '',
// company_id: 2942,
// group_id: 1247,
// type: 'in',
// from: '+380636414032',
// to: '80044340202',
// time: 1567419756609,

// event: 'Track',
// privilege: 'call,all',
// accountcode: 'gateway:2698',
// channel: 'SIP/0443002000-000186fe',
// channelid: '1567417667.247926',
// callid: '1567417667.247926',
// sessionid: '1567417667.247926',
// callerid: '380636414032',
// calleeid: '380443002067',
// realcalleeid: '380443002067',
// direction: 'answer',
// devicestate: 'UNKNOWN',
// callback: '0',
// webphoneaccountcode: '',
// company_id: 2942,
// group_id: 1247,
// type: 'in',
// external_num: '380443002067',
// from: '380636414032',
// time: 1567417667902,

// Hangup A param  {
//     from: '380636414032',
//     to: '80044340201',
//     time: '1567435084428',
//     sessionid: '1567435084.464354'
//   }
//   Hangup A all_data {
//     event: 'Hangup',
//     privilege: 'call,all',
//     channel: 'SIP/80044340201-00032c13',
//     uniqueid: '1567435084.464357',
//     calleridnum: '380636414032',
//     calleridname: '<unknown>',
//     connectedlinenum: '380636414032',
//     connectedlinename: '<unknown>',
//     accountcode: 'user:9436',
//     cause: '16',
//     'cause-txt': 'Normal clearing',
//     initiator: 'System',
//     company_id: 2942,
//     group_id: 1247,
//     type: 'in',
//     from: '380636414032',
//     to: '80044340201',
//     time: 1567435090809,
//     int: {
//       name: 'moysklad',
//       company_id: 2942,
//       content_type: 'application/json',
//       web_hook: 'https://online.moysklad.ru/api/phone/1.0',
//       client_key: '6f2a63d1ea9d8d1b283cb13edd4b8eb3278b9354'
//     }

//   Hangup no A all_data {
//     event: 'Hangup',
//     privilege: 'call,all',
//     channel: 'SIP/80044340202-00032c14',
//     uniqueid: '1567435084.464358',
//     calleridnum: '380636414032',
//     calleridname: '<unknown>',
//     connectedlinenum: '380636414032',
//     connectedlinename: '<unknown>',
//     accountcode: 'user:9437',
//     cause: '26',
//     'cause-txt': 'Answered elsewhere',
//     initiator: 'System',
//     company_id: 2942,
//     group_id: 1247,
//     type: 'in',
//     from: '380636414032',
//     to: '80044340202',
//     time: 1567435087518,
//     int: {
//       name: 'moysklad',
//       company_id: 2942,
//       content_type: 'application/json',
//       web_hook: 'https://online.moysklad.ru/api/phone/1.0',
//       client_key: '6f2a63d1ea9d8d1b283cb13edd4b8eb3278b9354'
//     }

exports.save_key = async (data) => {
    //    console.log(data);
    let key = 'eve_m:' + data.company_id + ':' + data.type + ':' + data.event + ':' + (await (data.channelid === undefined) ? data.uniqueid : data.channelid);
    let val = data.from + ',' + (await (data.external_num === undefined) ? data.to : data.external_num) + ',' + data.time + ',' + (await (data.sessionid === undefined) ? data.uniqueid : data.sessionid);
    // + ',' + data.sessionid;
    console.log('red key ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.save_transfer = async (from, data5) => {
    console.log(data5);
    //let key = 'eve_m:' + data5.company_id + ':' + data5.event + ':' + (await (data5.targetuniqueid === undefined) ? data5.uniqueid : data5.targetuniqueid);
    let key = 'eve_m:' + data5.company_id + ':' + data5.event;
    //let val = data5.from + ',' + data5.time + ',' + (await (data5.targetuniqueid === undefined) ? data5.uniqueid : data5.targetuniqueid) + ',' + data5.transferexten;
    let val = data5.from + ',' + data5.time + ',' + data5.uniqueid + ',' + data5.transferexten + ',' + data5.targetuniqueid;
    console.log('red key TRANSFER ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

// exports.save_hang = async (from, data, data4) => {
//     //    console.log(data);
//     let key = 'eve_m:' + data.type + ':' + data.event + ':' + await data.channelid + ':' + await data.callid;
//     let val = data.from + ',' + (await (data.external_num === undefined) ? data.to : data.external_num) + ',' + data.time + ',' + (await (data.sessionid === undefined) ? data.uniqueid : data.sessionid);
//     console.log('red key ' + key + ' | val ' + val);
//     await setAsync(key, val);
//     await expireAsync(key, rconf.key_exp);
//     return 1;
// }

exports.save_res = async (from, data, data2) => {

    //console.log('---------------------------------- saveres DATA ', data2);
    let key = 'eve_m:' + from + ':' + (await (data2.uniqueid == undefined) ? data2.channelid : data2.uniqueid);
    let val = data.result.CALL_ID;
    //+ ',' + data.result.CRM_CREATED_LEAD + ',' + data.result.CRM_ENTITY_ID + ',' + data.result.CRM_ENTITY_TYPE;
    console.log('red key ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.save_res_tr = async (from, data, data7) => {

    //console.log('---------------------------------- save res DATA RES TR', data7);
    let key = 'eve_m:' + from + ':' + data7.uniqueid;
    let val = data.result.CALL_ID;
    //+ ',' + data.result.CRM_CREATED_LEAD + ',' + data.result.CRM_ENTITY_ID + ',' + data.result.CRM_ENTITY_TYPE;
    console.log('red key RES TR ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.save_user = async (from, data, data6) => {

    console.log('data from save_user', data);
    let key = 'eve_m:' + from + ':' + (await (data6.uniqueid == undefined) ? data6.channelid : data6.uniqueid);
    let val = data.result[0].ID;
    //+ ',' + data.result.CRM_CREATED_LEAD + ',' + data.result.CRM_ENTITY_ID + ',' + data.result.CRM_ENTITY_TYPE;
    console.log('red key ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.save_user_tr = async (from, data, data8) => {

    //console.log('---------------------------------------------------data from save_user_TR', data);
    let key = 'eve_m:' + from + ':' + data8.uniqueid;
    let val = data.result[0].ID;
    //+ ',' + data.result.CRM_CREATED_LEAD + ',' + data.result.CRM_ENTITY_ID + ',' + data.result.CRM_ENTITY_TYPE;
    //console.log('----------------------------------------------------- USER TRred key ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.save_click = async (from, data9) => {
        //console.log('-------------------------------------------------------------------',data9);
    let key = 'eve_m:' + from + ':' + (await (data9.uniqueid == undefined) ? data9.sessionid : data9.uniqueid);
    let val = data9.from + ',' + data9.time + ',' + data9.accountcode + ',' + data9.uniqueid;
    //console.log('---------------------------------------red key click2call ' + key + ' | val ' + val);
    await setAsync(key, val);
    await expireAsync(key, rconf.key_exp);
    return 1;
}

exports.get_click_param = async (from, data10) => {

    let key = 'eve_m:' + from + ':' + (await (data10.uniqueid == undefined) ? data10.sessionid : data10.uniqueid);
    console.log('get_click_param ' + key);
    let dat = await getAsync(key);
    //console.log('data from get click param ' + dat);
    if (dat) {
        let send = {
            "accountcode": dat.split(',')[2],
            "uniqueid": dat.split(',')[3],
        }
        return send;
    }
    else { return null; }
}

exports.get_user_param = async (from, data6) => {

    let key = 'eve_m:' + from + ':' + (await (data6.uniqueid == undefined) ? data6.channelid : data6.uniqueid);
    console.log('get_user_param ' + key);
    let dat = await getAsync(key);
    //console.log('data from get USER param ' + dat);
    if (dat) {
        let send = {
            "ID": dat.split(',')[0],
        }
        return send;
    }
    else { return null; }
}

exports.get_user_tr_param = async (from, data8) => {

    let key = 'eve_m:' + from + ':' + data8.uniqueid;
    //console.log('------------------------------------get_user_tr_param ' + key);
    let dat = await getAsync(key);
    //console.log('-----------------------------------data from get USER TR param ' + dat);
    if (dat) {
        let send = {
            "ID": dat.split(',')[0],
        }
        return send;
    }
    else { return null; }
}

exports.get_key_param = async (from, data) => {
    let key = 'eve_m:' + data.company_id + ':' + data.type + ':' + from + ':' + (await (data.uniqueid === undefined) ? data.sessionid : data.uniqueid);
       console.log('-get_key_param '+key);
    let dat = await getAsync(key);
    if (dat) {
        let send = {
            "from": dat.split(',')[0],
            "to": dat.split(',')[1],
            "time": dat.split(',')[2],
            "sessionid": dat.split(',')[3],
            //"sessionid_for_c2c": dat.split(',')[4],
        }
        return send;
    }
    else { return null; }
}

exports.get_transfer_param = async (from, data5) => {
    //let key = 'eve_m:' + data5.from + ':' + data5.time + ':' + data5.targetuniqueid + ':' + data5.transferexten;
    let key = 'eve_m:' + data5.company_id + ':' + from;
    console.log('-------- get_transfer_param ' + key);
    let dat = await getAsync(key);
    //console.log('-------- get_transfer_param DAT ' + dat);
    if (dat) {
        let send = {
            "from": dat.split(',')[0],
            "to": dat.split(',')[0],
            "transferexten": dat.split(',')[3],
            "targetuniqueid": dat.split(',')[4],
        }
        return send;
    }
    else { return null; }
}

exports.get_res_param = async (from, data2) => {

    let key = 'eve_m:' + from + ':' + (await (data2.uniqueid == undefined) ? data2.channelid : data2.uniqueid);
       console.log('-get_res_param '+key);
    let dat = await getAsync(key);
    console.log('data from get res param' + dat);
    if (dat) {
        let send = {
            "CALL_ID": dat.split(',')[0],
            // "CRM_CREATED_LEAD": dat.split(',')[1],
            // "CRM_ENTITY_ID": dat.split(',')[2],
            // "CRM_ENTITY_TYPE": dat.split(',')[3]
        }
        return send;
    }
    else { return null; }
}

exports.get_res_tr_param = async (from, data7) => {

    let key = 'eve_m:' + from + ':' + data7.uniqueid;
    console.log('get_res_TR_param ' + key);
    let dat = await getAsync(key);
    //console.log('----------------------------------------------data from get res TR param' + dat);
    if (dat) {
        let send = {
            "CALL_ID": dat.split(',')[0],
            // "CRM_CREATED_LEAD": dat.split(',')[1],
            // "CRM_ENTITY_ID": dat.split(',')[2],
            // "CRM_ENTITY_TYPE": dat.split(',')[3]
        }
        return send;
    }
    else { return null; }
}

// exports.get_hang_param = async (from, data) => {
//     let key = 'eve_m:' + uniqueid + ':' + time;
//     console.log('-----get_channelid_param '+key);
//     let dat = await getAsync(key);
//     console.log('-----get_channelid_param ', dat);
//     if (dat) {
//         let send = {
//             "channelid": dat.split(',')[0],
//             "time": dat.split(',')[1]
//         }
//         return send;
//     }
//     else { return null; }
// }

exports.get_hang_param = async (from, data4) => {
    let key = 'eve_m:'+from+':'+ data4.type + ':' + from + ':' + data4.callid + ':' + data4.sessionid;
       console.log('-get_key_param '+key);
    let dat = await getAsync(key);
    if (dat) {
        let send = {
            "from": dat.split(',')[0],
            "to": dat.split(',')[1],
            "time": dat.split(',')[2],
            "sessionid": dat.split(',')[3],
            "callid": dat.split(',')[4]
        }
        return send;
    }
    else { return null; }
}