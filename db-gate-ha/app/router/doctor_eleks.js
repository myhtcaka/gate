const routes = require('express').Router();
const check = require('../middleware/check');
const doctor_eleks = require('../controller/doctor_eleks');

//routes.post('/audio_record_params',[check.authentication],gate_back.audio_record_params);
routes.post('/integrations_data', [check.authentication], doctor_eleks.integrations_data);
routes.post('/streamtele_change_clients', [check.authentication], doctor_eleks.streamtele_change_clients);
routes.post('/streamtele_get_doctoreleks_id', [check.authentication], doctor_eleks.streamtele_get_doctoreleks_id);
routes.post('/streamtele_get_clients_phones', [check.authentication], doctor_eleks.streamtele_get_clients_phones);
routes.post('/streamtele_get_client_info_channels', [check.authentication], doctor_eleks.streamtele_get_client_info_channels);
routes.post('/user_internal_by_uid', [check.authentication], doctor_eleks.user_internal_by_uid);

// user_internal_by_uid(user_internal_by_uid)
//clients/streamtele_change_clients
// /clients/streamtele_get_doctoreleks_id (customer_id)
// /clients/streamtele_get_clients_phones (customer_id)
// /clients/streamtele_get_client_info_channels (info_channels_id)

module.exports = routes;