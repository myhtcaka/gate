const app = require("express")();
const fs = require('fs');
const https = require('https');
const bodyParser = require('body-parser');

const set = require('./servconf').conf;
const ami_sip = require('./app/router/ami_sip');
const ami_sipu = require('./app/router/ami_sipu');
const ami_sip_v3 = require('./app/router/ami_sip_v3');
const kyivstar_iskrate = require('./app/router/kyivstar_iskrate');



//const int_data = require('./app/controller/services/mariadb');
const int_data = require('./app/controller/services/reqwest');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({ type: 'text/html' }));

app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Authorization ,X-CSRF-Token,X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
    res.header("Access-Control-Allow-Methods", "POST, GET");
//    console.log(req);
    next();
});

(async () => {
    let int_datas = await int_data.get_integrations();
    datas = async (req, res, next) => {
        req.body.int = int_datas.data;
        int_datas.time = await (int_datas.time === undefined) ? Date.now() : int_datas.time;
        int_datas = await (Date.now() - int_datas.time) > 60000 ? await int_data.get_integrations() : int_datas;
        next();
    }

    app.use('/ami_sip', datas, ami_sip);
    app.use('/ami_sipu', datas, ami_sipu);
    app.use('/ami_sip_v3', datas, ami_sip_v3);
    app.use('/kyivstar_iskrate', kyivstar_iskrate);
    

    https.createServer({
        key: fs.readFileSync(set.cer.key),
        cert: fs.readFileSync(set.cer.cert)
    }, app)
        .listen(set.post, set.host, function () {
            console.log('App listening go to https://' + set.host + ':' + set.post);
        });

})();