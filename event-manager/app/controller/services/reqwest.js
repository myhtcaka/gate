const request = require('request-promise');
const keys = require('./keys');
const urls = require('../../../servconf').urls;

// ---- db --
// exports.all_internal_num = async () => {
//   const key = await keys.get_one_time_key();
//   //console.log('key ',key);
//   const options = {
//     headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + key },
//     uri: urls.db_gate+ '/data-internal',
//     json: true,
//     body: { "test":"test01" },
//     method: 'POST',
//     rejectUnauthorized: false,
// };
//   // console.log(options);
//   try {
//     const response = await request(options);
//    // console.log('response ',response);
//    console.log('send ok');
   
//     return response;
//    // if(response.user_is=='true'){return '1';}
//    // else{return '0';}
    
//   }
//   catch (error) {
//     console.log('error ', error);
//    console.log('send error');
    
//     return '0';
//   }
// }





exports.get_integrations = async () => {
  
  const key = await keys.get_one_key();
  console.log('key ',key);
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + key },
    uri: urls.db_gate+ '/integrations_data',
    json: true,
    body: { "test":"test01" },
    method: 'POST',
    rejectUnauthorized: false,
};
  // console.log(options);
  try {
    const response = await request(options);
   // console.log('response ',response);
   console.log('send get_integrations ok');
  // console.log('get_integrations ', response);
    return response;
   // if(response.user_is=='true'){return '1';}
   // else{return '0';}
    
  }
  catch (error) {
    console.log('error ', error);
   console.log('send get_integrations error');
    
    return '0';
  }
}
