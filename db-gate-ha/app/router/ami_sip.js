const routes = require('express').Router();
const check = require('../middleware/check');
const ami_sip = require('../controller/ami_sip');

routes.post('/data-internal', [check.authentication], ami_sip.data_all_internal_num);
routes.post('/data-external', [check.authentication], ami_sip.data_all_external_num);
// data-all-internal-num
module.exports = routes;