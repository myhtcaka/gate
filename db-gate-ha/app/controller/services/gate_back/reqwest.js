
const request = require('request-promise');
const keys = require('../keys');
const urls = require('../../../../servconf').urls;


exports.audio_record_params = async (sess) => {
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/audio_record_params',
    json: true,
    body: { sess },
    method: 'POST',
    rejectUnauthorized: false,
  };
  //    console.log('options ',options);
  try {
    const response = await request(options);
    // console.log('response ',response);
    console.log('send ok');
    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}
  }
  catch (error) {
    //    console.log('error ', error);
    console.log('send error');
    return '0';
  }
}

exports.softswitch_by_key = async (key) => {
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/softswitch_by_key',
    json: true,
    body: { key },
    method: 'POST',
    rejectUnauthorized: false,
  };
  console.log('options ', options);
  try {
    const response = await request(options);
    console.log('response ', response);
    console.log('send ok');
    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}
  }
  catch (error) {
    //    console.log('error ', error);
    console.log('send error');
    return '0';
  }
}

exports.lic_extifces_by_key = async (key) => {
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/lic_extifces_by_key',
    json: true,
    body: { key },
    method: 'POST',
    rejectUnauthorized: false,
  };
  console.log('options ', options);
  try {
    const response = await request(options);
    console.log('response ', response);
    console.log('send ok');
    return response;
    // if(response.user_is=='true'){return '1';}
    // else{return '0';}
  }
  catch (error) {
    //    console.log('error ', error);
    console.log('send error');
    return '0';
  }
}
// /params/active_channel_uniqueid(req.body.uniqueid)
exports.active_channel_uniqueid = async (uniqueid) => {
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/active_channel_uniqueid',
    json: true,
    body: { uniqueid },
    method: 'POST',
    rejectUnauthorized: false,
  };
  console.log('options ', options);
  try {
    const response = await request(options);
    // console.log('response ', response);
    console.log('send ok active_channel_uniqueid ');
    return response;
  }
  catch (error) {
    //    console.log('error ', error);
    console.log('send error active_channel_uniqueid');
    return '0';
  }
}

// /params/crm_id_internal(req.body.crm_id)
exports.crm_id_internal = async (crm_id) => {
  const bkey = await keys.get_one_time_key();
  const options = {
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + bkey },
    uri: 'https://' + urls.db_agent + '/params/crm_id_internal',
    json: true,
    body: { crm_id },
    method: 'POST',
    rejectUnauthorized: false,
  };
  console.log('options ', options);
  try {
    const response = await request(options);
    // console.log('response ', response);
    console.log('send ok crm_id_internal ');
    return response;
  }
  catch (error) {
    //    console.log('error ', error);
    console.log('send error crm_id_internal');
    return '0';
  }
}
