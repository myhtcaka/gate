const prostiezvonki = require('./integrations/prostiezvonki');
const retailcrm = require('./integrations/retailcrm');
const istat24 = require('./integrations/istat24');
const streamtele = require('./integrations/streamtele');
const alfacrm = require('./integrations/alfacrm');
const moysklad = require('./integrations/moysklad/moysklad');
const yclients = require('./integrations/yclients/yclients');
const bitrix24 = require('./integrations/bitrix24/bitrix24');
const inteltech_1c = require('./integrations/inteltech_1c/inteltech_1c');
const doctorEleks_V = require('./integrations/doctorEleks_V/doctorEleks_V');

exports.ami_sip_all = async (req, res) => {
    // console.log("Processing func -> ami_sip_all");
    //  console.log(req.body);
    let arrFound = await (req.body.int).filter((item) => { if (item.company_id === req.body.company_id) { return item; } });
    //   console.log('-----------my integratios----------',req.body );
    if (arrFound.length != 0) {
        //   console.log('-----------my integratios----------',arrFound );
        arrFound.forEach(el1 => {
             //   console.log('send to ', el1);
            //   prostiezvonki.(el1.name)(req.body);
            if (el1.name === 'prostiezvonki') { prostiezvonki.prostiezvonki(req.body, el1); }
            else if (el1.name === 'retailcrm') { retailcrm.retailcrm(req.body, el1); }
            else if (el1.name === 'istat24') { istat24.istat24(req.body, el1); }
            else if (el1.name === 'streamtele') { streamtele.streamtele(req.body, el1); }
            else if (el1.name === 'alfacrm') { alfacrm.alfacrm(req.body, el1); }
            else if (el1.name === 'moysklad') { req.body.int = el1; moysklad.moysklad(req.body); }
            else if (el1.name === 'yclients') {req.body.int = el1; yclients.yclients(req.body, el1); }
            else if (el1.name === 'bitrix24') {req.body.int = el1; bitrix24.bitrix24(req.body, el1); }
            else if (el1.name === '1c-inteltech') {req.body.int = el1; inteltech_1c.inteltech_1c(req.body, el1); }
            else if (el1.name === 'doctorEleks-V') { req.body.int = el1; doctorEleks_V.doctorEleks_V(req.body, el1); }
        });
    }
}