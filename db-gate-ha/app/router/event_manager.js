const routes = require('express').Router();
const check = require('../middleware/check');
const event_manager = require('../controller/event_manager');

//routes.post('/audio_record_params',[check.authentication],gate_back.audio_record_params);
routes.post('/integrations_data', [check.authentication], event_manager.integrations_data);

module.exports = routes;