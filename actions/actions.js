const app = require("express")();
const fs = require('fs');
const https = require('https');
const bodyParser = require('body-parser');

const set = require('./servconf').host;

const gate_back = require('./app/router/gate_back');
const event_manager = require('./app/router/event_manager');

app.use(bodyParser.json());


app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Authorization , Accept, Content-Length");
    res.header("Access-Control-Allow-Methods", "POST, GET");
//    console.log(req);
    next();
});


app.use('/gate-back', gate_back);
app.use('/event-manager', event_manager);

https.createServer({
    key: fs.readFileSync(set.cer.key),
    cert: fs.readFileSync(set.cer.cert)
}, app)
    .listen(set.post, set.host, function () {
        console.log('App listening go to https://' + set.host + ':' + set.post);
    });